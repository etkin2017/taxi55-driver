package in.org.cyberspace.taxi55_driver;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ActiveBookingDetail extends AppCompatActivity {

    ImageView back_arrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_booking_detail);

        back_arrow = (ImageView) findViewById(R.id.back_arrow);


        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

       /* Bundle extra= getIntent().getExtras();
        String ID=extra.getString("ID");
        Log.i("ID",ID);
        getDetails(ID);*/
    }

    public void getDetails(final String ID) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("unique_id", ID);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final ProgressDialog dialog = new ProgressDialog(ActiveBookingDetail.this);
        dialog.setCancelable(false);
        dialog.setMessage("loading...");
        dialog.show();
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "get_booking_detail", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                dialog.dismiss();

                Log.i("Response", response.toString());

                TextView tvID = (TextView) findViewById(R.id.tvID);
                TextView tvUsername = (TextView) findViewById(R.id.tvUsername);
                TextView tvEmail = (TextView) findViewById(R.id.tvEmail);
                TextView tvPhone = (TextView) findViewById(R.id.tvMobile);
                TextView tvFrom = (TextView) findViewById(R.id.tvFrom);
                TextView tvTo = (TextView) findViewById(R.id.tvTo);
                TextView tvTime = (TextView) findViewById(R.id.tvTime);
                TextView tvDistance = (TextView) findViewById(R.id.tvDist);
                TextView tvAmt = (TextView) findViewById(R.id.tvAmt);
                TextView tvDate = (TextView) findViewById(R.id.tvDate);
                TextView tvAdd = (TextView) findViewById(R.id.tvAdd);

                tvID.setText("Booking ID : " + ID);
                try {
                    JSONArray data = response.getJSONArray("data");
                    JSONObject response1 = data.getJSONObject(0);
                    tvUsername.setText(response1.getString("username"));
                    tvEmail.setText(response1.getString("email"));
                    tvPhone.setText(response1.getString("mobile"));
                    tvFrom.setText(response1.getString("pickup_area"));
                    tvTo.setText(response1.getString("drop_area"));
                    tvDate.setText(response1.getString("pickup_date"));
                    tvTime.setText(response1.getString("pickup_time"));
                    tvDistance.setText(response1.getString("distance"));
                    tvAmt.setText(response1.getString("amount"));
                    tvAdd.setText(response1.getString("pickup_address"));
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.printStackTrace();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }
}
