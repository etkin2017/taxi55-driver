package in.org.cyberspace.taxi55_driver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import in.org.cyberspace.taxi55_driver.adapter.ActiveBookingAdapter;

public class ActiveBookingList extends AppCompatActivity {
    ArrayList<String> id;
    ArrayList<String> from;
    ArrayList<String> to;
    ArrayList<String> amt;
    ArrayList<String> km;
    ArrayList<String> date;
    ArrayList<String> time;
    ArrayList<String> assigned_for;

    ImageView back_arrow;

    ArrayList<String> username;

    ArrayList<String> fcm;
    ArrayList<String> phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_active_booking_list);

        id = new ArrayList<String>();
        from = new ArrayList<String>();
        to = new ArrayList<String>();
        amt = new ArrayList<String>();
        km = new ArrayList<String>();
        date = new ArrayList<String>();
        time = new ArrayList<String>();
        username = new ArrayList<>();
        assigned_for = new ArrayList<>();

        fcm = new ArrayList<String>();
        phone = new ArrayList<>();

        back_arrow = (ImageView) findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fetchBookings();
    }


    public void fetchBookings() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait...");
        dialog.show();
        PrefManager prefManager = new PrefManager(ActiveBookingList.this);
        String Did = prefManager.getID();

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("driver_id", Did);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "driver_bookings", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                Log.e("PassengerResponse", response.toString());
                try {
                    JSONArray jsonArray = response.getJSONArray("new_rade");


                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        if (jsonObject1.getString("status").equals("Processing")) {
                            id.add(jsonObject1.getString("uneaque_id"));
                            from.add(jsonObject1.getString("pickup_area"));
                            to.add(jsonObject1.getString("drop_area"));
                            date.add(jsonObject1.getString("pickup_date"));
                            time.add(jsonObject1.getString("pickup_time"));
                            amt.add(jsonObject1.getString("amount"));
                            km.add(jsonObject1.getString("km"));
                            username.add(jsonObject1.getString("username"));
                            fcm.add(jsonObject1.getString("fcm"));
                            phone.add(jsonObject1.getString("mobile"));
                        }
                    }

                    BookingListAdapter adapter = new BookingListAdapter(ActiveBookingList.this, id, date, time, amt, km, from, to, username, fcm, phone,assigned_for);
                    ListView listView = (ListView) findViewById(R.id.listView1);
                    listView.setDivider(null);
                    listView.setAdapter(adapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.printStackTrace();
            }
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }

   /* public void fetchBookings() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait...");
        dialog.show();
        PrefManager prefManager = new PrefManager(this);
        String Did = prefManager.getID();

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("driver_id", Did);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "driver_bookings", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                Log.i("Response", response.toString());
                try {

                    JSONArray jsonArray = response.getJSONArray("new_rade");


                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        if (jsonObject1.getString("status").equals("Processing") || jsonObject1.getString("status").equals("Complete")) {
                            id.add(jsonObject1.getString("uneaque_id"));
                            from.add(jsonObject1.getString("pickup_area"));
                            to.add(jsonObject1.getString("drop_area"));
                            date.add(jsonObject1.getString("pickup_date"));
                            time.add(jsonObject1.getString("pickup_time"));
                            amt.add(jsonObject1.getString("amount"));
                            km.add(jsonObject1.getString("status"));
                            Log.i("Amt", jsonObject1.getString("amount"));
                        }
                    }

                    ActiveBookingAdapter activeBookingAdapter = new ActiveBookingAdapter((Activity) ActiveBookingList.this, id, date, time, amt, km, from, to);
                    ListView listView = (ListView) findViewById(R.id.listView1);
                    listView.setDivider(null);
                    listView.setAdapter(activeBookingAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.printStackTrace();
            }
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }*/

}
