package in.org.cyberspace.taxi55_driver;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class AddressActivity extends AppCompatActivity implements OnMapReadyCallback {

    ImageView back_arrow;
    GoogleMap mMap;
    SupportMapFragment mapFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);

        back_arrow = (ImageView) findViewById(R.id.back_arrow);

        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.address_map);
        mapFragment.getMapAsync(this);


        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                startActivity(new Intent(AddressActivity.this ,Login.class));
             //   onBackPressed();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(28.3835, 36.5662))
                .title("Taxi55 B.O.Box 10053, Zip Code 71433, Tabuk Alnaseem")
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA)));


        marker.showInfoWindow();

        LatLng pickup_lat_lng = new LatLng(28.3835, 36.5662);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(pickup_lat_lng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));

    }

    private int calculateZoomLevel() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        double equatorLength = 40075004; // in meters
        double widthInPixels = width;
        double metersPerPixel = equatorLength / 256;
        int zoomLevel = 1;
        while ((metersPerPixel * widthInPixels) > 3000) {
            metersPerPixel /= 2;
            ++zoomLevel;
        }
        Log.i("ADNAN", "zoom level = " + zoomLevel);
        return zoomLevel;
    }

}
