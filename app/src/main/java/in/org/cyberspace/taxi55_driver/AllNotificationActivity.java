package in.org.cyberspace.taxi55_driver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import in.org.cyberspace.taxi55_driver.adapter.AllNotificationAdapter;
import in.org.cyberspace.taxi55_driver.application.MyPreferenceManager;
import in.org.cyberspace.taxi55_driver.db.NotiDbHelper;
import in.org.cyberspace.taxi55_driver.model.RequestMessage;

public class AllNotificationActivity extends AppCompatActivity {

    ImageView back_arrow;
    RecyclerView recycler_all_notification;
    TextView clear_all;
    NotiDbHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_notification);
        back_arrow = (ImageView) findViewById(R.id.back_arrow);
        clear_all = (TextView) findViewById(R.id.clear_all);
        dbHelper = new NotiDbHelper(this);
        recycler_all_notification = (RecyclerView) findViewById(R.id.recycler_all_notification);
        recycler_all_notification.setLayoutManager(new LinearLayoutManager(this));
        try {

            List<RequestMessage> requestMessageList = dbHelper.getAllPassgenerMessage();
            AllNotificationAdapter adapter = new AllNotificationAdapter(requestMessageList, AllNotificationActivity.this);
            recycler_all_notification.setAdapter(adapter);

            String id = "from_user";
            MyPreferenceManager preferenceManager = new MyPreferenceManager(this);
            preferenceManager.removeUserNGroupNotificaion(id);
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }

        clear_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dbHelper.delete_all_values();

                List<RequestMessage> requestMessageList = dbHelper.getAllPassgenerMessage();
                AllNotificationAdapter adapter = new AllNotificationAdapter(requestMessageList, AllNotificationActivity.this);
                recycler_all_notification.setAdapter(adapter);
            }
        });


        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
