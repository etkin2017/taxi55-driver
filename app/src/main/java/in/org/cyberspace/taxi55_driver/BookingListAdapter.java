package in.org.cyberspace.taxi55_driver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.sql.Time;
import java.util.ArrayList;

/**
 * Created by Wasim on 13/02/2017.
 */

public class BookingListAdapter extends BaseAdapter {

    ArrayList<String> ID, Dates, Times, Amt, KM, FROM, TO, USERNAME, FCM, PHONE, assigned_for;
    Activity mContex;

    public BookingListAdapter(Activity context, ArrayList<String> id, ArrayList<String> dates, ArrayList<String> times, ArrayList<String> amt, ArrayList<String> km, ArrayList<String> from, ArrayList<String> to, ArrayList<String> USERNAME, ArrayList<String> FCM, ArrayList<String> PHONE, ArrayList<String> assigned_for) {
        this.mContex = context;
        this.ID = id;
        this.Dates = dates;
        this.Times = times;
        this.Amt = amt;
        this.KM = km;
        this.FROM = from;
        this.TO = to;
        this.USERNAME = USERNAME;
        this.FCM = FCM;
        this.PHONE = PHONE;
        this.assigned_for = assigned_for;
    }

    public BookingListAdapter(Activity context, ArrayList<String> id, ArrayList<String> dates, ArrayList<String> times, ArrayList<String> amt, ArrayList<String> km, ArrayList<String> from, ArrayList<String> to) {
        this.mContex = context;
        this.ID = id;
        this.Dates = dates;
        this.Times = times;
        this.Amt = amt;
        this.KM = km;
        this.FROM = from;
        this.TO = to;


    }

    @Override
    public int getCount() {
        return ID.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = mContex.getLayoutInflater();
        View row = layoutInflater.inflate(R.layout.booking_list, viewGroup, false);
        final TextView tvID, tvDate, tvTime, tvAmt, tvKM, tvFrom, tvTo, usnm, fcm, phone_nm, assigned_for1;

        tvID = (TextView) row.findViewById(R.id.tvID);
        tvDate = (TextView) row.findViewById(R.id.tvDate);
        tvTime = (TextView) row.findViewById(R.id.tvTime);
        tvAmt = (TextView) row.findViewById(R.id.tvAmount);
        tvKM = (TextView) row.findViewById(R.id.tvDist);
        tvFrom = (TextView) row.findViewById(R.id.tvFrom);
        tvTo = (TextView) row.findViewById(R.id.tvTo);
        usnm = (TextView) row.findViewById(R.id.usnm);
        assigned_for1 = (TextView) row.findViewById(R.id.assigned_for);

        fcm = (TextView) row.findViewById(R.id.fcm);
        phone_nm = (TextView) row.findViewById(R.id.phone_nm);

     /*   tvID.setText(ID.get(i));
        tvDate.setText("Date : "+Dates.get(i));
        tvTime.setText("Time : "+Times.get(i));
        tvAmt.setText("Amount : "+Amt.get(i));
        tvKM.setText("Distance : "+KM.get(i));
        tvFrom.setText("From : "+FROM.get(i));
        tvTo.setText("To : "+TO.get(i));*/


        tvID.setText(ID.get(i));
        tvDate.setText("" + Dates.get(i));
        tvTime.setText("" + Times.get(i));
        tvAmt.setText("Amt. : " + Amt.get(i) + " Rs");
        tvKM.setText("Dist. : " + KM.get(i) + " Km");
        tvFrom.setText("" + FROM.get(i));
        tvTo.setText("" + TO.get(i));
        try {
            usnm.setText(USERNAME.get(i));
            fcm.setText(FCM.get(i));
            phone_nm.setText(PHONE.get(i));
            assigned_for1.setText(assigned_for.get(i));

        } catch (Exception ee) {
            ee.printStackTrace();
        }
        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String _ID = ID.get(i);
              /*  Intent intent = new Intent(mContex, ActiveBookingDetail.class);
                intent.putExtra("ID", _ID);
                mContex.startActivity(intent);*/

                Bundle b = new Bundle();
                b.putString("date", tvDate.getText().toString());
                b.putString("id", tvID.getText().toString());
                b.putString("time", tvTime.getText().toString());
                b.putString("amt", tvAmt.getText().toString().replaceFirst("Amt. : ", ""));
                b.putString("km", tvKM.getText().toString().replaceFirst("Dist. : ", ""));
                b.putString("from", tvFrom.getText().toString());
                b.putString("to", tvTo.getText().toString());
                b.putString("uname", usnm.getText().toString());
                b.putString("phone", phone_nm.getText().toString());
                b.putString("fcm", fcm.getText().toString());
                b.putString("user_id", assigned_for1.getText().toString());


                Intent intent = new Intent(mContex, MapActivity.class);
                intent.putExtras(b);
                mContex.startActivity(intent);
            }
        });

        return row;

    }
}
