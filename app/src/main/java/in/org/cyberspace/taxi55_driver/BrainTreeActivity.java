/*
package in.org.cyberspace.taxi55_driver;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.braintreegateway.BraintreeGateway;
import com.braintreegateway.CustomerRequest;
import com.braintreegateway.Environment;
import com.braintreegateway.Request;
import com.braintreegateway.Result;
import com.braintreegateway.Transaction;
import com.braintreegateway.TransactionOptionsRequest;
import com.braintreegateway.TransactionRequest;
import com.braintreegateway.ValidationError;
import com.braintreegateway.ValidationErrors;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Logger;

import cz.msebera.android.httpclient.client.methods.RequestBuilder;
import okhttp3.Response;
import okhttp3.Route;

import static cz.msebera.android.httpclient.client.methods.RequestBuilder.post;


public class BrainTreeActivity extends AppCompatActivity {


    String clienTok = "sandbox_2qwfptwb_bdx4tx599b2qdmz7";
    private static final String TAG = MainActivity.class.getSimpleName();
    private static final String PATH_TO_SERVER = "http://uncletaxi.sarodestudio.photography/";
    private String clientToken = "sandbox_2qwfptwb_bdx4tx599b2qdmz7";
    private static final int BRAINTREE_REQUEST_CODE = 4949;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brain_tree);



        gateway = connectBraintreeGateway();
        braintreeProcessing();


    }


    public void onBraintreeSubmit(View view) {
        DropInRequest dropInRequest = new DropInRequest().clientToken(clientToken);
        try {
            startActivityForResult(dropInRequest.getIntent(this), BRAINTREE_REQUEST_CODE);
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BRAINTREE_REQUEST_CODE) {
            if (RESULT_OK == resultCode) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                String paymentNonce = result.getPaymentMethodNonce().getNonce();
                //send to your server
                Log.d(TAG, "Testing the app here");
                sendPaymentNonceToServer(paymentNonce);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Log.d(TAG, "User cancelled payment");
            } else {
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                Log.d(TAG, " error exception");
            }
        }
    }

    private void sendPaymentNonceToServer(String paymentNonce) {
        RequestParams params = new RequestParams("NONCE", paymentNonce);
        AsyncHttpClient androidClient = new AsyncHttpClient();
        androidClient.post(PATH_TO_SERVER, params, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                Log.d(TAG, "Error: Failed to create a transaction");
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                Log.d(TAG, "Output " + responseString);
            }


        });
    }


    private static Logger logger = Logger.getLogger(TAG);

    // Below are the Braintree sandbox credentials
    private static BraintreeGateway gateway = null;
    private static String publicKey = "5jjxj2gq7pdtzskn";
    private static String privateKey = "cd85eae0724534b5459810e2fdead839";
    private static String merchantId = "bdx4tx599b2qdmz7";

    public static void braintreeProcessing() {

        System.out.println(" ----- BrainTree Implementation Starts --- ");

        // Generate client Token
        generateClientToken();

    }

    // Connect to Braintree Gateway.
    public static BraintreeGateway connectBraintreeGateway() {
        BraintreeGateway braintreeGateway = new BraintreeGateway(
                Environment.SANDBOX, merchantId, publicKey, privateKey);
        return braintreeGateway;
    }

    // Make an endpoint which return client token.
    public static void generateClientToken() {
        // client token will be generated at server side and return to client
        final String[] ct = {""};
        new AsyncTask() {
            String clientToken = "";

            @Override
            protected Object doInBackground(Object[] params) {
                clientToken = gateway.clientToken().generate();
               // ct=clientToken;
                return clientToken;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                ct[0] =clientToken;

                System.out.println(" Client Token : " + clientToken);

                // Receive payment method nonce
                String nonceFromTheClient = receivePaymentMethodNonce();

                // Do payment transactions
                BigDecimal amount = new BigDecimal("5.10");
                doPaymentTransaction(nonceFromTheClient, amount);
            }
        }.execute();



      //  ct[0];
    }

    // Make an endpoint which receive payment method nonce from client and do payment.
    public static String receivePaymentMethodNonce() {
        String nonceFromTheClient = "fake-valid-mastercard-nonce";
        return nonceFromTheClient;
    }

    // Make payment
    public static void doPaymentTransaction(String paymentMethodNonce, BigDecimal amount) {

        final TransactionRequest request = new TransactionRequest();
        request.amount(amount);
        request.paymentMethodNonce(paymentMethodNonce);

        CustomerRequest customerRequest = request.customer();
        customerRequest.email("cpatel@gmail.com");
        customerRequest.firstName("Chirag");
        customerRequest.lastName("Patel");

        TransactionOptionsRequest options = request.options();
        options.submitForSettlement(true);

        // Done the transaction request
        options.done();

        // Create transaction ...

        new AsyncTask() {
            Result<Transaction> result =null;
            @Override
            protected Object doInBackground(Object[] params) {
               result = gateway.transaction().sale(request);
                return result;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                boolean isSuccess = result.isSuccess();
                if (isSuccess) {
                    Transaction transaction = result.getTarget();
                    displayTransactionInfo(transaction);
                } else {
                    ValidationErrors errors = result.getErrors();
                    validationError(errors);
                }
            }
        }.execute();


    }

    private static void displayTransactionInfo(Transaction transaction) {
        System.out.println(" ------ Transaction Info ------ ");
        System.out.println(" Transaction Id  : " + transaction.getId());
        System.out.println(" Processor Response Text : " + transaction.getProcessorResponseText());
    }

    private static void validationError(ValidationErrors errors) {
        List<ValidationError> error = errors.getAllDeepValidationErrors();
        for (ValidationError er : error) {
            System.out.println(" error code : " + er.getCode());
            System.out.println(" error message  : " + er.getMessage());
        }
    }

}
*/
