package in.org.cyberspace.taxi55_driver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CancelledBookings extends AppCompatActivity {

    ArrayList<String> id;
    ArrayList<String> from;
    ArrayList<String> to;
    ArrayList<String> amt;
    ArrayList<String> km;
    ArrayList<String> date;
    ArrayList<String> time;

    ImageView back_arrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_cancelled_bookings);
        id = new ArrayList<String>();
        from = new ArrayList<String>();
        to = new ArrayList<String>();
        amt = new ArrayList<String>();
        km = new ArrayList<String>();
        date = new ArrayList<String>();
        time = new ArrayList<String>();


        back_arrow =(ImageView) findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        fetchBookings();
    }

    public void fetchBookings()
    {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait...");
        dialog.show();
        PrefManager prefManager = new PrefManager(this);
        String Did = prefManager.getID();

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("driver_id",Did);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "driver_bookings", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                Log.i("Response",response.toString());
                try {

                    JSONArray jsonArray = response.getJSONArray("Cancelled");



                    for (int i = 0 ;i<jsonArray.length();i++)
                    {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        id.add(jsonObject1.getString("uneaque_id"));
                        from.add(jsonObject1.getString("pickup_area"));
                        to.add(jsonObject1.getString("drop_area"));
                        date.add(jsonObject1.getString("pickup_date"));
                        time.add(jsonObject1.getString("pickup_time"));
                        amt.add(jsonObject1.getString("amount"));
                        km.add(jsonObject1.getString("km"));
                        Log.i("Amt",jsonObject1.getString("amount"));
                    }

                    PostBookingListAdapter postBookingListAdapter = new PostBookingListAdapter((Activity)CancelledBookings.this,id,date,time,amt,km,from,to);
                    ListView listView =(ListView)findViewById(R.id.listView1);
                    listView.setDivider(null);
                    listView.setAdapter(postBookingListAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.printStackTrace();
            }
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest,"BookingRequest");
    }
}
