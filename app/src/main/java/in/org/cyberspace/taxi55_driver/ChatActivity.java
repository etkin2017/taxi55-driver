package in.org.cyberspace.taxi55_driver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import in.org.cyberspace.taxi55_driver.application.Taxi55;
import in.org.cyberspace.taxi55_driver.db.NotiDbHelper;
import in.org.cyberspace.taxi55_driver.model.User;
import in.org.cyberspace.taxi55_driver.services.NotificationReceiver;


public class ChatActivity extends AppCompatActivity {

    List<User> userList;

    NotiDbHelper mHelper;
    RecyclerView contrecy;
    RecentChatRoomAdapter mAdaptor;
   /* MyNewBroadcast broadcast;*/

    ImageView back_arrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        mHelper = new NotiDbHelper(Taxi55.getInstance());
        contrecy = (RecyclerView) findViewById(R.id.contrecyc);
        back_arrow =(ImageView) findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
/*
        broadcast = new MyNewBroadcast();
        IntentFilter filter = new IntentFilter(NotificationReceiver.name2);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        try {
            registerReceiver(broadcast, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();

        userList = mHelper.getUserByChat();
        mAdaptor = new RecentChatRoomAdapter(userList, ChatActivity.this);
        contrecy.setLayoutManager(new LinearLayoutManager(ChatActivity.this));
        contrecy.setAdapter(mAdaptor);
    }

    public void refresh() {


        ArrayList<User> ur = mHelper.getUserByChat();
        mAdaptor = new RecentChatRoomAdapter(ur, Taxi55.getInstance());

        mAdaptor.setUserList(ur);
        contrecy.setAdapter(mAdaptor);
        mAdaptor.notifyDataSetChanged();
    }


   /* public class MyNewBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            //  String bd = intent.getExtras().getString("data");
            refresh();
            ArrayList<User> ur = mHelper.getUserByChat();
            if (ur == null) {
                ur = new ArrayList<User>();
            } else {

                mAdaptor = new RecentChatRoomAdapter(ur, ChatActivity.this);
                mAdaptor.setUserList(ur);
            }


            try {
                //  mAdaptor.msgcnt(1);
                mAdaptor.setUserList(ur);
            } catch (Exception e) {
                e.printStackTrace();
            }
            contrecy.setLayoutManager(new LinearLayoutManager(ChatActivity.this));
            contrecy.setAdapter(mAdaptor);


        }
    }*/
}
