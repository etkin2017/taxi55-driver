package in.org.cyberspace.taxi55_driver;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.HashMap;

/**
 * Created by Wasim on 12/02/2017.
 */

public class DBHelper extends SQLiteOpenHelper {
    Context mcontext;
    final static String DB_NAME = "TAXI55_DB";
    final String USER_TABLE = "USER_DATA";


    public DBHelper(Context context) {
        super(context, DB_NAME, null, 1);
        this.mcontext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("Create table " + USER_TABLE + " (username text primary key,name text, password text,email text,mobile text,id text);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("Drop Table if exist " + USER_TABLE);
        onCreate(sqLiteDatabase);
    }

    public void saveUserData(String username, String name, String password, String email, String mobile, String token) {

        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("Delete from " + USER_TABLE);
        ContentValues values = new ContentValues();
        values.put("username", username);
        values.put("name", name);
        values.put("email", email);
        values.put("password", password);
        values.put("mobile", mobile);
        values.put("id", token);

        db.insert(USER_TABLE, null, values);
    }

    public Cursor getUserData() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("Select * from " + USER_TABLE, null);
        return c;
    }

    public HashMap<String, String> get_userData() {
        HashMap<String, String> nmap = new HashMap<>();

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery("Select * from " + USER_TABLE, null);
        try {

            if (c.moveToFirst()) {
                nmap.put("username", c.getString(0));
                nmap.put("name", c.getString(1));
                nmap.put("email", c.getString(3));
                nmap.put("mobile", c.getString(4));
                // nmap.put("password", c.getString(3));
             /*   nmap.put("mobile", c.getString(4));
                nmap.put("id", c.getString(5));
            }*/

            //    nmap.put("id", c.getString(5));
            }
        } catch (Exception ee) {
            ee.printStackTrace();
        }
        return nmap;
    }




}
