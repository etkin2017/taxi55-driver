package in.org.cyberspace.taxi55_driver;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by Wasim on 20/02/2017.
 */

public class FragmentBookingCancel extends Fragment {
    ArrayList<String> id;
    ArrayList<String> from;
    ArrayList<String> to;
    ArrayList<String> amt;
    ArrayList<String> km;
    ArrayList<String> date;
    ArrayList<String> time;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.frag_post_booking_cancel, container, false);

        from=getArguments().getStringArrayList("From");
        to=getArguments().getStringArrayList("To");
        date=getArguments().getStringArrayList("Date");
        time=getArguments().getStringArrayList("Time");
        km=getArguments().getStringArrayList("KM");
        id=getArguments().getStringArrayList("ID");
        amt=getArguments().getStringArrayList("amt");




        ListView listView = (ListView) v.findViewById(R.id.listView1);
        BookingListAdapter adapter = new BookingListAdapter(getActivity(),id,date,time,amt,km,from,to);

        listView.setAdapter(adapter);

        return v;

    }
}
