package in.org.cyberspace.taxi55_driver;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.readystatesoftware.viewbadger.BadgeView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import in.org.cyberspace.taxi55_driver.application.Taxi55;
import in.org.cyberspace.taxi55_driver.services.MyService;
import in.org.cyberspace.taxi55_driver.services.NotificationReceiver;

public class Home extends AppCompatActivity
        implements AdapterView.OnItemClickListener {

    ArrayList<String> id;
    ArrayList<String> from;
    ArrayList<String> to;
    ArrayList<String> amt;
    ArrayList<String> km;
    ArrayList<String> date;
    ArrayList<String> time;
    ArrayList<String> username;
    ListView menudrawer_list;
    ArrayList<String> fcm;
    ArrayList<String> phone;
    ArrayList<String> assigned_for;
    Spinner driver_status;

    String[] meniListArray = {"Active Bookings", "Completed Bookings", "Cancelled Bookings", "Subscription", "My Profile", "Chat",
            "Logout", "Exit"};

    View v;
    BadgeView badge;

    View target;

    List<String> list;
    TextView menu_profile_emailid, menu_profile_name;
    MyNewBroadcast broadcast;
    TextView no_active_boooking;

    public static boolean home_activate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        no_active_boooking = (TextView) findViewById(R.id.no_active_boooking);
        id = new ArrayList<String>();
        from = new ArrayList<String>();
        to = new ArrayList<String>();
        amt = new ArrayList<String>();
        km = new ArrayList<String>();
        date = new ArrayList<String>();
        time = new ArrayList<String>();
        username = new ArrayList<>();
        assigned_for = new ArrayList<>();
        fcm = new ArrayList<String>();
        phone = new ArrayList<>();

        home_activate = true;
        broadcast = new MyNewBroadcast();
        IntentFilter filter = new IntentFilter(NotificationReceiver.name3);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        try {
            registerReceiver(broadcast, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }

        startService(new Intent(Home.this, MyService.class));
      /*  LayoutInflater layoutInflater = mContex.getLayoutInflater();*/
        //    View row=layoutInflater.inflate(R.layout.booking_list,viewGroup,false);


        try {
            SharedPreferences sh = getSharedPreferences("fcm_token", MODE_PRIVATE);
            String refreshedToken = sh.getString("fcm_token", "");
            Log.e("refreshedToken", refreshedToken);
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        fetchBookings();

        Toast.makeText(getApplicationContext(), " Welcome To Taxi55 ", Toast.LENGTH_LONG).show();


        menudrawer_list = (ListView) findViewById(R.id.drawer_list);
        driver_status = (Spinner) findViewById(R.id.spin_status);


        list = Arrays.asList(getResources().getStringArray(R.array.driver_status));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, R.layout.status_driver, list);
        driver_status.setAdapter(dataAdapter);

        driver_status.setSelection(0);

        try {
            target = findViewById(R.id.noti);
            badge = new BadgeView(getApplicationContext(), target);
            badge.setBadgePosition(BadgeView.POSITION_TOP_LEFT);
            badge.setBadgeBackgroundColor(Color.DKGRAY);
            badge.setBadgeMargin(5);

            badge.setText("2");
            badge.show();
        } catch (Exception eq) {
            eq.printStackTrace();
        }


        try {

            findViewById(R.id.noti).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(Home.this, AllNotificationActivity.class));
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
     /*   driver_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tx = (TextView) v.findViewById(R.id.text1);
                tx.setTextColor(Color.BLACK);
            }
        });*/


        driver_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String status = list.get(position);
                Log.e("STATUS", status);
                updateStatus(status, "0");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        menudrawer_list.setAdapter(new LeftMenuItemAdapter(this, Arrays
                .asList(meniListArray)));

        menudrawer_list.setOnItemClickListener(this);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        LinearLayout imageView = (LinearLayout) findViewById(R.id.drawer_lin_lay);
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.dwr_crop);
        Bitmap blurredBitmap = blur(bitmap);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(blurredBitmap);
        imageView.setBackgroundDrawable(bitmapDrawable);

      /*  NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
*/
        DBHelper db = new DBHelper(getApplicationContext());
        HashMap<String, String> userData = db.get_userData();


        //   View v = navigationView.getHeaderView(0);
        menu_profile_emailid = (TextView) findViewById(R.id.menu_profile_emailid);
        menu_profile_name = (TextView) findViewById(R.id.menu_profile_name);


        try {

            menu_profile_name.setText(userData.get("name") + "");
            menu_profile_emailid.setText(userData.get("email") + "");
            // String  ak = prefManager.get

        } catch (Exception ee) {
            ee.printStackTrace();
        }

        getStatus();

        startService(new Intent(this, LocationService.class));


    }


    public void addBadge() {
        try {

            String a = "";
            int b = 0;

            String id = "from_user";

            String oldNotification = Taxi55.getInstance().getPrefManager().getNotificationsByGroup(id);
            List<String> messages = Arrays.asList(oldNotification.split("\\|"));

            Set<String> unique = new HashSet<String>(messages);
            for (String key : unique) {
                System.out.println(key + ": " + Collections.frequency(messages, key));
                a = key;
                b = Collections.frequency(messages, key);

            }


            if (b > 0) {
                badge.setText(b + "");
                badge.show();
            } else {
                badge.hide();

            }

        } catch (Exception e) {
            badge.hide();
            e.printStackTrace();
        }
    }


    public void getStatus() {

        //http://uncletaxi.sarodestudio.photography/web_service/get_driver_status

        final ProgressDialog pdlg = new ProgressDialog(Home.this);
        pdlg.setMessage("Please Wait...");
        pdlg.setCancelable(false);
        pdlg.show();
        String REQUEST_TAG = getPackageName() + ".get_driver_status";

        PrefManager pre_manager = new PrefManager(Home.this);
        String d_id = pre_manager.getID();

        final JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("driver_id", d_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "get_driver_status", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                pdlg.dismiss();
                Log.e("PassengerResponse", response.toString());
                try {
                    JSONArray jsonArray = response.getJSONArray("driver_status");
                    JSONObject dri_state = jsonArray.getJSONObject(0);

                    String ss = dri_state.getString("status");
                    int u = list.indexOf(ss);

                    driver_status.setSelection(u);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pdlg.dismiss();
                error.printStackTrace();
            }
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");


    }

    public void updateStatus(String status, final String a) {
        final ProgressDialog pdlg = new ProgressDialog(Home.this);
        pdlg.setMessage("Please Wait...");
        pdlg.setCancelable(false);
        pdlg.show();
        String REQUEST_TAG = getPackageName() + ".update_driver_status";

        PrefManager pre_manager = new PrefManager(Home.this);
        String d_id = pre_manager.getID();

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("status", status);
            jsonObject.put("driver_id", d_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "update_driver_status", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                pdlg.dismiss();

                Log.i("Response", response.toString());

                if (a.equals("1")) {
                    finish();
                }

                try {
                    String data = response.getString("status");
                    Log.e("STATUS_DATA", data);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), "Network Problem , Please Try Again", Toast.LENGTH_SHORT).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                pdlg.dismiss();
                error.printStackTrace();
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");


    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
/*

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_act_booking) {
            // Handle the camera action
        } else if (id == R.id.nav_complete_booking) {
            startActivity(new Intent(Home.this, CompletedBookings.class));
        } else if (id == R.id.nav_cancel_booking) {
            startActivity(new Intent(Home.this, CancelledBookings.class));
        } else if (id == R.id.nav_subscriptions) {

        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(Home.this, Profile.class));
        } else if (id == R.id.nav_logout) {
            logout();
        } else if (id == R.id.nav_exit) {
            exit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
*/

    public void logout() {
        AlertDialog.Builder dlg = new AlertDialog.Builder(Home.this);
        dlg.setTitle(R.string.app_name);
        dlg.setMessage("Are You Sure You Want To Logout ?");
        dlg.setIcon(R.drawable.icon);
        dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                PrefManager prefManager = new PrefManager(Home.this);
                prefManager.setUserName("");
                prefManager.setIsUserLoggedIn(false);
                updateStatus("offline", "1");
                //   finish();


            }
        });


        dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                return;
            }
        });

        dlg.create().show();
    }

    public void updateStatusLog(String status) {
        final ProgressDialog pdlg = new ProgressDialog(Home.this);
        pdlg.setMessage("Please Wait...");
        pdlg.setCancelable(false);
        pdlg.show();
        String REQUEST_TAG = getPackageName() + ".update_driver_status";

        PrefManager pre_manager = new PrefManager(Home.this);
        final String d_id = pre_manager.getID();


        StringRequest postRequest = new StringRequest(Request.Method.POST, ConnectionConfiguration.URL + "update_driver_status", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                pdlg.dismiss();
                try {
                    JSONObject hs = new JSONObject(response);

                    String sat = hs.getString("status");


                    finish();
                } catch (Exception e) {


                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pdlg.dismiss();
                        // Show timeout error message


                    }
                }) {
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-type", "text/plain");
                params.put("driver_id", d_id);
                params.put("status", "offline");
                return params;
            }
        };
        int socketTimeout = 50000;
        postRequest.setRetryPolicy(new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(postRequest, REQUEST_TAG);

        // Adding request to request queue


    }

    @Override
    protected void onResume() {
        super.onResume();

        if (ActivityCompat.checkSelfPermission(Home.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Home.this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.READ_CONTACTS},
                    110);

        }
        home_activate = true;

        addBadge();
    }

    @Override
    protected void onStop() {
        super.onStop();
        home_activate = false;
    }


    public void exit() {
        AlertDialog.Builder dlg = new AlertDialog.Builder(Home.this);
        dlg.setTitle(R.string.app_name);
        dlg.setMessage("Are You Sure You Want To Exit ?");
        dlg.setIcon(R.drawable.icon);
        dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
                updateStatus("offline", "0");

                home_activate = false;
            }
        });


        dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                return;
            }
        });

        dlg.create().show();
    }


    public void fetchBookings() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait...");
        dialog.show();
        PrefManager prefManager = new PrefManager(Home.this);
        String Did = prefManager.getID();

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("driver_id", Did);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "driver_bookings", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                Log.e("PassengerResponse", response.toString());
                try {
                    JSONArray jsonArray = response.getJSONArray("new_rade");


                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        id.add(jsonObject1.getString("uneaque_id"));
                        from.add(jsonObject1.getString("pickup_area"));
                        to.add(jsonObject1.getString("drop_area"));
                        date.add(jsonObject1.getString("pickup_date"));
                        time.add(jsonObject1.getString("pickup_time"));
                        amt.add(jsonObject1.getString("amount"));
                        km.add(jsonObject1.getString("km"));
                        username.add(jsonObject1.getString("username"));
                        fcm.add(jsonObject1.getString("fcm"));
                        phone.add(jsonObject1.getString("mobile"));
                        assigned_for.add(jsonObject1.getString("id"));
                    }

                    if (jsonArray.length() != 0) {

                        BookingListAdapter adapter = new BookingListAdapter(Home.this, id, date, time, amt, km, from, to, username, fcm, phone, assigned_for);
                        ListView listView = (ListView) findViewById(R.id.listView1);
                        listView.setDivider(null);
                        listView.setAdapter(adapter);
                    } else {
                        no_active_boooking.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.printStackTrace();
            }
        });

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);


        switch (position) {
            case 0:  // Handle the camera action
                startActivity(new Intent(Home.this, ActiveBookingList.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 1:
                startActivity(new Intent(Home.this, CompletedBookings.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 2:
                startActivity(new Intent(Home.this, CancelledBookings.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 3:
                startActivity(new Intent(Home.this, SubScriptionActivity.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 4:
                startActivity(new Intent(Home.this, Profile.class));
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 5:
                startActivity(new Intent(Home.this, ChatActivity.class));
                drawer.closeDrawer(GravityCompat.START);
                break;

            case 6:
                logout();
                drawer.closeDrawer(GravityCompat.START);
                break;
            case 7:
                exit();
                drawer.closeDrawer(GravityCompat.START);
                break;


        }



      /*  if (id == R.id.nav_act_booking) {
            // Handle the camera action
        } else if (id == R.id.nav_complete_booking) {
            startActivity(new Intent(Home.this, CompletedBookings.class));
        } else if (id == R.id.nav_cancel_booking) {
            startActivity(new Intent(Home.this, CancelledBookings.class));
        } else if (id == R.id.nav_subscriptions) {

        } else if (id == R.id.nav_profile) {
            startActivity(new Intent(Home.this, Profile.class));
        } else if (id == R.id.nav_logout) {
            logout();
        } else if (id == R.id.nav_exit) {
            exit();
        }*/

       /* drawer.closeDrawer(GravityCompat.START);*/


    }


    public class MyNewBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            // String bd = intent.getExtras().getString("data1");
            try {
                addBadge();
            } catch (Exception ea) {
                ea.printStackTrace();
            }
            //Log.e("badge_data", bd);


            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds

                v.vibrate(1000);
                r.play();

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    class LeftMenuItemAdapter extends BaseAdapter {


        private List<String> originalData = null;
        private LayoutInflater mInflater;
        ArrayList<Integer> itemsimg = new ArrayList<Integer>();

        public LeftMenuItemAdapter(Context context, List<String> delarnames) {
            this.originalData = delarnames;
            mInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemsimg.add(R.drawable.taxi);
            itemsimg.add(R.drawable.book_complete);
            itemsimg.add(R.drawable.book_cancel);
            itemsimg.add(R.drawable.subscription);
            itemsimg.add(R.drawable.user);
            itemsimg.add(R.drawable.chatt);

            itemsimg.add(R.drawable.logout);
            itemsimg.add(R.drawable.exit);


        }


        @Override
        public int getCount() {
            return originalData.size();
        }

        @Override
        public Object getItem(int position) {
            return originalData.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LeftMenuItemAdapter.ViewHolder holder = null;
            if (convertView == null) {
                convertView = mInflater
                        .inflate(R.layout.drawer_menu_item, null);
                holder = new LeftMenuItemAdapter.ViewHolder();

                holder.ic = (ImageView) convertView
                        .findViewById(R.id.imageView);
                holder.menu_Name = (TextView) convertView
                        .findViewById(R.id.menu_name_tv);
//
                //  holder.menu_Name.setTypeface(regular);

                convertView.setTag(holder);
                holder.menu_Name.setTag(R.id.menu_name_tv, position);
                holder.ic.setTag(R.id.imageView, position);

               /* if(position ==3){
                    holder.menu_Name.setVisibility(View.GONE);
                    holder.ic.setVisibility(View.GONE);
                }*/



            } else {
                holder = (LeftMenuItemAdapter.ViewHolder) convertView.getTag();
            }
            holder.menu_Name.setText(originalData.get(position));
            holder.ic.setImageResource(itemsimg.get(position));
            holder.ic.setColorFilter(Color.WHITE);


            return convertView;
        }

        class ViewHolder {
            ImageView ic;
            TextView menu_Name;
        }
    }


    private static final float BLUR_RADIUS = 25f;

    public Bitmap blur(Bitmap image) {
        if (null == image) return null;

        Bitmap outputBitmap = Bitmap.createBitmap(image);
        final RenderScript renderScript = RenderScript.create(this);
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

//Intrinsic Gausian blur filter
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }
}
