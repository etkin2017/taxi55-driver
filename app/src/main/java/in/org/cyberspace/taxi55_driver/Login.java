package in.org.cyberspace.taxi55_driver;

import android.*;
import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Looper;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.org.cyberspace.taxi55_driver.application.DAO;
import in.org.cyberspace.taxi55_driver.application.Taxi55;
import in.org.cyberspace.taxi55_driver.model.GPSTracker;
import in.org.cyberspace.taxi55_driver.model.Subscription;


public class Login extends AppCompatActivity {

    List<Subscription> sub_plan;
    String plan_id = "";
    String price_nm = "";
    String plan_nm = "";
    ImageView car_no_img, license_no_img, other_img;
    Bitmap bm_car_no, bm_licension_no, bm_other_docs;
    String license_no, car_no;
    String encoded_string_lice_nm = "", encoded_string_car_nm = "", encoded_string_other_doc = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sub_plan = new ArrayList<Subscription>();
      //  getSupscription();
    }

    public void login(View v) {
      /*  GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {*/
        EditText txtUser = (EditText) findViewById(R.id.input_username);
        EditText txtPass = (EditText) findViewById(R.id.input_password);


        final String username = txtUser.getText().toString().trim();
        final String pass = txtPass.getText().toString().trim();


        if (username.equals("")) {
            txtUser.setError("Enter Username !");
            txtUser.requestFocus();
            return;
        }

        if (pass.equals("")) {
            txtPass.setError("Enter Password !");
            txtPass.requestFocus();
            return;
        }

        final ProgressDialog pdlg = new ProgressDialog(Login.this);
        pdlg.setMessage("Please Wait...");
        pdlg.setCancelable(false);
        pdlg.show();
        String REQUEST_TAG = getPackageName() + ".login_request";
        final JSONObject jsonObject = new JSONObject();
        try {

            SharedPreferences sh = getSharedPreferences("fcm_token", MODE_PRIVATE);
            String refreshedToken = sh.getString("fcm_token", "");

            Log.e("refreshedToken", refreshedToken);

/*

            try{
                refreshedToken = FirebaseInstanceId.getInstance().getToken();
            }catch (Exception e1){
                e1.printStackTrace();
            }
*/

            jsonObject.put("secret_key", ConnectionConfiguration.SECRET_KEY);
            jsonObject.put("Email", username);
            jsonObject.put("Password", pass);
            jsonObject.put("fcm", refreshedToken);
            jsonObject.put("status", "online");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(ConnectionConfiguration.URL + "driver_login", jsonObject, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.i("Response", response.toString());
                pdlg.dismiss();
                try {
                    JSONObject jsonObject1 = response.getJSONObject(0);


                    if (jsonObject1.getString("status").equals("success")) {

                        if (jsonObject1.getString("user_status").equalsIgnoreCase("active")) {

                            PrefManager prefManager = new PrefManager(Login.this);
                            prefManager.setUserName(username);
                            prefManager.setIsUserLoggedIn(true);
                            prefManager.setUserID(jsonObject1.getString("id"));
                            DBHelper db = new DBHelper(Login.this);
                            db.saveUserData(jsonObject1.getString("username"), jsonObject1.getString("name"), "", jsonObject1.getString("email"), jsonObject1.getString("mobile"), jsonObject1.getString("id"));
                            //  db.saveUserData(jsonObject1.getString("username"),jsonObject1.getString("name"),);


                            String lat1 = jsonObject1.getString("latitude");
                            String lng1 = jsonObject1.getString("longitude");

                            if (lat1.equals("null") && lng1.equals("null")) {
                                startActivity(new Intent(Login.this, Profile.class));
                            } else {
                                startActivity(new Intent(Login.this, Home.class));
                            }

                            finish();

                        } else {
                            finish();
                            startActivity(new Intent(Login.this, AddressActivity.class));

                        }
                    } else {
                        Toast.makeText(Login.this, jsonObject1.getString("message"), Toast.LENGTH_SHORT).show();
                        return;
                    }

                } catch (JSONException e) {
                    Toast.makeText(Login.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pdlg.dismiss();
                        error.printStackTrace();
                        Log.i("Response", error.toString());
                    }
                });
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonArrayRequest, REQUEST_TAG);

       /* } else {
            // can't get location
            // GPS or Network is not enabled
            // Ask user to enable GPS/network in settings
            showSettingsAlert();

        }*/
    }

    private void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }


    public void getSupscription() {


        String REQUEST_TAG = getPackageName() + ".getSubPlans";
        final JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("Email", "abc");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url1 = ConnectionConfiguration.URL + "fetch_sub_plans";
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(url1, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Response", response.toString());

                try {
                    // JSONObject jsonObject1 = response.getJSONObject(0);
                    if (response.getString("status").equals("success")) {


                        try {
                            JSONArray res1 = response.getJSONArray("data");


                            for (int i = 0; i < res1.length(); i++) {

                                Subscription subscription = new Subscription(res1.getJSONObject(i).getString("id"),
                                        res1.getJSONObject(i).getString("price")
                                        , res1.getJSONObject(i).getString("name"));

                                sub_plan.add(subscription);
                            }

                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }


                    } else {
                        Toast.makeText(Login.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (JSONException e) {
                    Toast.makeText(Login.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();
                        Log.i("Response", error.toString());
                    }
                });
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonArrayRequest, REQUEST_TAG);


    }


    public int getIndex(String itemName) {
        for (int i = 0; i < sub_plan.size(); i++) {
            Subscription auction = sub_plan.get(i);
            if (itemName.equals(auction.getName())) {
                return i;
            }
        }

        return -1;
    }

    @Override
    protected void onResume() {
        super.onResume();


        if (ActivityCompat.checkSelfPermission(Login.this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(Login.this, new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE},
                    110);

        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == 1) {
            try {


                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                        null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();

                String selectedImagePath = cursor.getString(column_index);


                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
              /*  Bitmap bm = BitmapFactory.decodeFile(selectedImagePath, options);
                car_no_img.setImageBitmap(bm);*/

                // car_no_img.setImageDrawable();


                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
                    //bm_car_no =bitmap;
                    car_no_img.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                File file = new File(selectedImagePath);
                BitmapFactory.Options options1 = new BitmapFactory.Options();
                options1.inSampleSize = 4;
                // Bitmap bitmap = BitmapFactory.decodeFile(selectedImagePath, options1);
                Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options1);
                bm_car_no = bitmap;

                if (byteSizeOf(bitmap) > 921600) {

                    options1 = new BitmapFactory.Options();
                    options1.inSampleSize = 8;
                    //   bitmap = BitmapFactory.decodeFile(selectedImagePath, options1);
                    bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options1);
                    bm_car_no = bitmap;
                }
                if (byteSizeOf(bitmap) > 2097152) {

                    options1 = new BitmapFactory.Options();
                    options1.inSampleSize = 12;
                    //  bitmap = BitmapFactory.decodeFile(selectedImagePath, options1);
                    bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options1);
                    bm_car_no = bitmap;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        if (resultCode == RESULT_OK && requestCode == 2) {


            try {


                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                        null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();

                String selectedImagePath = cursor.getString(column_index);


                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                //    Bitmap bm1 = BitmapFactory.decodeFile(selectedImagePath, options);
                  /*  license_no_img.setImageBitmap(BitmapFactory
                            .decodeFile(selectedImagePath));*/


                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
                    bm_licension_no = bitmap;
                    license_no_img.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                File file = new File(selectedImagePath);

                BitmapFactory.Options options1 = new BitmapFactory.Options();
                options1.inSampleSize = 4;
                Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options1);//BitmapFactory.decodeFile(selectedImagePath, options1);
                bm_licension_no = bitmap;

                if (byteSizeOf(bitmap) > 921600) {

                    options1 = new BitmapFactory.Options();
                    options1.inSampleSize = 8;
                    bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options1);//BitmapFactory.decodeFile(selectedImagePath, options1);
                    bm_licension_no = bitmap;
                }
                if (byteSizeOf(bitmap) > 2097152) {

                    options1 = new BitmapFactory.Options();
                    options1.inSampleSize = 12;
                    bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options1);//BitmapFactory.decodeFile(selectedImagePath, options1);
                    bm_licension_no = bitmap;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        if (resultCode == RESULT_OK && requestCode == 3) {

            try {
                Uri selectedImageUri = data.getData();
                String[] projection = {MediaStore.MediaColumns.DATA};
                Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                        null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();

                String selectedImagePath = cursor.getString(column_index);


                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(selectedImagePath, options);
                final int REQUIRED_SIZE = 200;
                int scale = 1;
                while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                        && options.outHeight / scale / 2 >= REQUIRED_SIZE)
                    scale *= 2;
                options.inSampleSize = scale;
                options.inJustDecodeBounds = false;
                //    Bitmap bm1 = BitmapFactory.decodeFile(selectedImagePath, options);
                  /*  license_no_img.setImageBitmap(BitmapFactory
                            .decodeFile(selectedImagePath));*/


                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), selectedImageUri);
                    bm_other_docs = bitmap;
                    other_img.setImageBitmap(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                File file = new File(selectedImagePath);

                BitmapFactory.Options options1 = new BitmapFactory.Options();
                options1.inSampleSize = 4;
                Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options1);// BitmapFactory.decodeFile(selectedImagePath, options1);
                bm_other_docs = bitmap;

                if (byteSizeOf(bitmap) > 921600) {

                    options1 = new BitmapFactory.Options();
                    options1.inSampleSize = 8;
                    bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options1);//BitmapFactory.decodeFile(selectedImagePath, options1);
                    bm_other_docs = bitmap;
                }
                if (byteSizeOf(bitmap) > 2097152) {

                    options1 = new BitmapFactory.Options();
                    options1.inSampleSize = 12;
                    bitmap = BitmapFactory.decodeStream(new FileInputStream(file), null, options1);//BitmapFactory.decodeFile(selectedImagePath, options1);
                    bm_other_docs = bitmap;
                }


            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    public void pickImageFromGalary_Car() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 1);
    }

    public void pickImageFromGalary_Licenson() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 2);
    }

    public void pickImageFromGallery_OtherDoc() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, 3);
    }

    public void signup(View v) {

     /*   final GPSTracker gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {*/

        //   final Dialog dialog = new Dialog(Login.this);

        final Dialog dialog = new Dialog(Login.this);

        dialog.setContentView(R.layout.signup_layout);

        dialog.setTitle("Signup...");

        // set the custom dialog components - text, image and button

        Button dialogButton = (Button) dialog.findViewById(R.id.singup_dialog_btn);
        car_no_img = (ImageView) dialog.findViewById(R.id.car_no_img);
        license_no_img = (ImageView) dialog.findViewById(R.id.license_no_img);
        other_img = (ImageView) dialog.findViewById(R.id.other_img);
      /*  car_no = (EditText) dialog.findViewById(R.id.car_no);
        license_no = (EditText) dialog.findViewById(R.id.license_no);*/


        car_no_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageFromGalary_Car();
            }
        });

        license_no_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageFromGalary_Licenson();
            }
        });

        other_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pickImageFromGallery_OtherDoc();
            }
        });


        final Spinner add_sub_scription = (Spinner) dialog.findViewById(R.id.add_sub_scription);


        List<String> list = new ArrayList<String>();
        if (list.isEmpty()) {
            for (int i = 0; i < sub_plan.size(); i++) {
                list.add(sub_plan.get(i).getName());
            }
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, list);
        //dataAdapter.setDropDownViewResource(R.layout.item_spinner);
        add_sub_scription.setAdapter(dataAdapter);

        try {
            add_sub_scription.setSelection(sub_plan.indexOf(0));

            int a = getIndex(sub_plan.get(0).getName());
            Subscription ii = sub_plan.get(a);
            add_sub_scription.setSelection(a);

            plan_id = sub_plan.get(0).getId();
            price_nm = sub_plan.get(0).getPrice();
            plan_nm = sub_plan.get(0).getName();

        } catch (Exception e) {
            e.printStackTrace();
        }

        add_sub_scription.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                add_sub_scription.setSelection(position);
                plan_id = sub_plan.get(position).getId();
                price_nm = sub_plan.get(position).getPrice();
                plan_nm = sub_plan.get(position).getName();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });// {


        // if button is clicked, close the custom dialog
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //startActivity(new Intent(Login.this,Home.class));
                //finish();
                EditText txtUser = (EditText) dialog.findViewById(R.id.input_username);
                EditText txtEmail = (EditText) dialog.findViewById(R.id.input_email);
                EditText txtMobile = (EditText) dialog.findViewById(R.id.input_mobile);
                EditText txtPass = (EditText) dialog.findViewById(R.id.input_password);
                EditText txtCPass = (EditText) dialog.findViewById(R.id.input_c_passworde);
                EditText txtName = (EditText) dialog.findViewById(R.id.input_name);


                final String username = txtUser.getText().toString().trim();
                final String name = txtName.getText().toString().trim();
                final String mobile = txtMobile.getText().toString().trim();
                final String email = txtEmail.getText().toString().trim();
                final String pass = txtPass.getText().toString().trim();
                String cPass = txtCPass.getText().toString().trim();
                try {

                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bm_car_no.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    byte[] array = stream.toByteArray();
                    encoded_string_car_nm = Base64.encodeToString(array, 0);

                    ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
                    bm_licension_no.compress(Bitmap.CompressFormat.JPEG, 100, stream1);
                    byte[] array1 = stream1.toByteArray();
                    encoded_string_lice_nm = Base64.encodeToString(array1, 0);

                    ByteArrayOutputStream stream2 = new ByteArrayOutputStream();
                    bm_other_docs.compress(Bitmap.CompressFormat.JPEG, 100, stream2);
                    byte[] array2 = stream2.toByteArray();
                    encoded_string_other_doc = Base64.encodeToString(array2, 0);


                } catch (Exception eq) {
                    eq.printStackTrace();
                }

                if (name.equals("")) {
                    txtName.setError("Enter Your Name !");
                    txtName.requestFocus();
                    return;
                } else if (username.equals("")) {
                    txtUser.setError("Enter Username !");
                    txtUser.requestFocus();
                    return;
                } else if (email.equals("")) {
                    txtEmail.setError("Enter Email !");
                    txtEmail.requestFocus();
                    return;
                } else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    txtEmail.setError("Enter Valid Email !");
                    txtEmail.requestFocus();
                    return;
                } else if (mobile.equals("")) {
                    txtMobile.setError("Enter Mobile Number !");
                    txtMobile.requestFocus();
                    return;
                } else if (pass.equals("")) {
                    txtPass.setError("Enter Password !");
                    txtPass.requestFocus();
                    return;
                } else if (cPass.equals("")) {
                    txtCPass.setError("Confirmn Password !");
                    txtCPass.requestFocus();
                    return;
                } else if (!pass.equals(cPass)) {
                    txtCPass.setError("Confirmn Password Did Not Match !");
                    txtCPass.requestFocus();
                    return;
                } else if (encoded_string_lice_nm.isEmpty()) {
                    Toast.makeText(Login.this, "Please attach Licenson document", Toast.LENGTH_SHORT).show();
                    return;
                } else if (encoded_string_other_doc.isEmpty()) {
                    Toast.makeText(Login.this, "Please attach Insurance document", Toast.LENGTH_SHORT).show();
                    return;
                } else if (encoded_string_car_nm.isEmpty()) {
                    Toast.makeText(Login.this, "Please attach Car plate Doc", Toast.LENGTH_SHORT).show();
                    return;
                } else {


                    final ProgressDialog pdlg = new ProgressDialog(Login.this);
                    pdlg.setMessage("Please Wait...");
                    pdlg.show();
                    pdlg.setCancelable(false);
                    String REQUEST_TAG = getPackageName() + ".volleyJsonArrayRequest";
                    JSONObject jsonObject = new JSONObject();
                    try {

                        SharedPreferences sh = getSharedPreferences("fcm_token", MODE_PRIVATE);
                        String refreshedToken = sh.getString("fcm_token", "");
                        Log.e("refreshedToken", refreshedToken);

                        jsonObject.put("secret_key", ConnectionConfiguration.SECRET_KEY);
                        jsonObject.put("Email", email);
                        jsonObject.put("Password", pass);
                        jsonObject.put("User_name", username);
                        jsonObject.put("Mobile", mobile);
                        jsonObject.put("Name", name);
                        jsonObject.put("sub_plan_id", plan_id);
                        jsonObject.put("fcm", refreshedToken);
                    /*    jsonObject.put("Latitude", gpsTracker.getLatitude());
                        jsonObject.put("Longitude", gpsTracker.getLongitude());*/


                        jsonObject.put("license_no", encoded_string_lice_nm);
                        jsonObject.put("car_no", encoded_string_car_nm);
                        jsonObject.put("document", encoded_string_other_doc);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "driver_sign_up", jsonObject, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.e("Response", response.toString());
                            pdlg.dismiss();
                            try {
                                if (response.getString("status").equals("success")) {

                                 //   finish();
                                    startActivity(new Intent(Login.this, AddressActivity.class));
                                 /*   DBHelper db = new DBHelper(Login.this);
                                    db.saveUserData(username, name, pass, email, mobile, response.getString("id"));
                                    PrefManager prefManager = new PrefManager(Login.this);
                                    prefManager.setIsUserLoggedIn(true);
                                    prefManager.setUserName(username);
                                    prefManager.setUserID(response.getString("id"));

                                    Intent ii = new Intent(Login.this, Home.class);*/
                                   /* Intent ii = new Intent(Login.this, MainActivity.class);
                                    ii.putExtra("price", price_nm);
                                    ii.putExtra("driver_id", response.getString("id"));
                                    ii.putExtra("plan_id", plan_id);
                                    ii.putExtra("plan_nm", plan_nm);*/


                                    //startActivity(ii);
                                    finish();
                                } else {
                                    Toast.makeText(Login.this, response.getJSONArray("error_list").getJSONObject(0).getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    error.printStackTrace();
                                    Log.i("Response", error.toString());
                                    pdlg.dismiss();
                                    Toast.makeText(Login.this, "NetWork Problem, Please Try Again", Toast.LENGTH_SHORT).show();
                                }
                            });
                    jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                            10000,
                            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                    AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, REQUEST_TAG);
                }
            }
        });


        dialog.show();
        Window window = dialog.getWindow();
        window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
       /* } else {
            showSettingsAlert();
        }*/
    }


    public int byteSizeOf(Bitmap bitmap) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return bitmap.getAllocationByteCount();
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1) {
            return bitmap.getByteCount();
        } else {
            return bitmap.getRowBytes() * bitmap.getHeight();
        }
    }

}
