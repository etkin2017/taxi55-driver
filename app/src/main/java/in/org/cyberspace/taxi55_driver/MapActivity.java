package in.org.cyberspace.taxi55_driver;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import in.org.cyberspace.taxi55_driver.application.DAO;

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    GoogleMap mMap;
    LatLng pickup_lat_lng, drop_lat_lng;
    String pickUrl = "";
    String dropUrl = "";

    Bundle rideData;
    String date, id, time, amt, km, from, to, uname, phone, fcm;
    TextView date_time, charges_txt, username, total_txt, from_txt, to_txt, id_txt;
    //get Location Latitude and Longitude
    //http://maps.googleapis.com/maps/api/geocode/json?address=1600+Amphitheatre+Parkway,+Mountain+View,+CA&sensor=true_or_false

    String mUrl = "http://maps.googleapis.com/maps/api/geocode/json?address=";

    ImageView back_arrow;
    JSONArray jsonArray, jsonArray1;
    JSONObject jsonObject, jsonObject1;
    // TextView start_chat_id;
    Button start_chat_id, track_user, call_btn;
    String track_user_id = "";

    public void init() {
        date_time = (TextView) findViewById(R.id.date_time);
        charges_txt = (TextView) findViewById(R.id.charges_txt);
        username = (TextView) findViewById(R.id.username);
        total_txt = (TextView) findViewById(R.id.total_txt);
        from_txt = (TextView) findViewById(R.id.from_txt);
        to_txt = (TextView) findViewById(R.id.to_txt);
        id_txt = (TextView) findViewById(R.id.id_txt);

        back_arrow = (ImageView) findViewById(R.id.back_arrow);

        start_chat_id = (Button) findViewById(R.id.start_chat_id);
        track_user = (Button) findViewById(R.id.track_user);
        call_btn = (Button) findViewById(R.id.call_btn);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        init();
        rideData = getIntent().getExtras();
        date = rideData.getString("date");//, tvDate.getText().toString());
        id = rideData.getString("id");//, tvID.getText().toString());
        time = rideData.getString("time");//, tvTime.getText().toString());
        amt = rideData.getString("amt");//, tvAmt.getText().toString());
        km = rideData.getString("km");//, tvKM.getText().toString());
        from = rideData.getString("from");//, tvFrom.getText().toString());
        to = rideData.getString("to");//, tvTo.getText().toString());
        uname = rideData.getString("uname");//, usnm.getText().toString());
        phone = rideData.getString("phone");
        fcm = rideData.getString("fcm");
        track_user_id = rideData.getString("user_id");
        try {
            pickUrl = mUrl + URLEncoder.encode(from, "UTF-8") + "&sensor=true";
            dropUrl = mUrl + URLEncoder.encode(to, "UTF-8") + "&sensor=true";
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        try {
            charges_txt.setText(amt);
            id_txt.setText(id);
            to_txt.setText(to);
            from_txt.setText(from);
            total_txt.setText(km);
            username.setText(uname);
        } catch (Exception ex1) {
            ex1.printStackTrace();
        }


        start_chat_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DBHelper dbHelper = new DBHelper(MapActivity.this);
                HashMap<String, String> mobile_data = dbHelper.get_userData();
                Intent new_int = new Intent(MapActivity.this, NewChatActivity.class);
                new_int.putExtra("rid", phone);
                new_int.putExtra("sid", mobile_data.get("mobile"));
                new_int.putExtra("fcm", fcm);
                new_int.putExtra("uname", uname);


                startActivity(new_int);

            }
        });

        track_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i1 = new Intent(MapActivity.this, UserTrackingActivity.class);
                i1.putExtra("user_id", track_user_id);
                i1.putExtra("booking_id", id);
                i1.putExtra("from", from.trim());
                i1.putExtra("to", to.trim());
                startActivity(i1);

            }
        });

        call_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_CALL);

                intent.setData(Uri.parse("tel:" + phone));
                if (ActivityCompat.checkSelfPermission(MapActivity.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                startActivity(intent);

            }
        });

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        try {
            String inputTimeStamp = date;

            final String inputFormat = "MM/dd/yyyy";
            final String outputFormat = " MMM dd  yyyy";

            String main_time = TimeStampConverter(inputFormat, inputTimeStamp, outputFormat);
            System.out.println(time + "");

            date_time.setText(main_time + " , " + time);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        getDesc();


    }

    public void getDesc() {
        new AsyncTask() {
            JSONObject resPick = null;
            JSONObject resDrop = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }


            @Override
            protected Object doInBackground(Object[] params) {
                resPick = DAO.getResult(pickUrl);
                resDrop = DAO.getResult(dropUrl);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);


                try {

                    jsonArray = resPick.getJSONArray("results");
                    jsonObject = jsonArray.getJSONObject(0);

                    JSONObject jsonObjectLatLng = jsonObject.getJSONObject("geometry");
                    jsonObjectLatLng = jsonObjectLatLng.getJSONObject("location");

                    String referenceOne = jsonObject.getString("formatted_address");
                    jsonArray = jsonObject.getJSONArray("address_components");


                    String latitude = jsonObjectLatLng.getString("lat");
                    String longitude = jsonObjectLatLng.getString("lng");

                    pickup_lat_lng = new LatLng((Double.parseDouble(latitude)), (Double.parseDouble(longitude)))
                    ;

                    try {
                        Log.e("jLat", latitude + "");
                        Log.e("jLng", longitude + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {

                    jsonArray1 = resDrop.getJSONArray("results");
                    jsonObject1 = jsonArray1.getJSONObject(0);

                    JSONObject jsonObjectLatLng1 = jsonObject1.getJSONObject("geometry");
                    jsonObjectLatLng1 = jsonObjectLatLng1.getJSONObject("location");

                    String referenceOne = jsonObject1.getString("formatted_address");
                    jsonArray1 = jsonObject1.getJSONArray("address_components");


                    String latitude1 = jsonObjectLatLng1.getString("lat");
                    String longitude1 = jsonObjectLatLng1.getString("lng");

                    drop_lat_lng = new LatLng((Double.parseDouble(latitude1)), Double.parseDouble(longitude1));
                    ;

                    try {
                        Log.e("jLat1", latitude1 + "");
                        Log.e("jLng1", longitude1 + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                String url1 = getDirectionsUrl(pickup_lat_lng, drop_lat_lng);

                DownloadTask task = new DownloadTask();
                task.execute(url1);


            }
        }.execute();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

    }


    private static String TimeStampConverter(final String inputFormat,
                                             String inputTimeStamp, final String outputFormat)
            throws ParseException {
        return new SimpleDateFormat(outputFormat).format(new SimpleDateFormat(
                inputFormat).parse(inputTimeStamp));
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {


        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Excep downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {


            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            if (result.size() < 1) {
                Toast.makeText(getApplicationContext(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {    // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duration = (String) point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.RED);
            }

            //   tvDistanceDuration.setText("Distance:"+distance + ", Duration:"+duration);
            Log.e("print_distance", "Distance:" + distance + ", Duration:" + duration);

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);

            MarkerOptions options = new MarkerOptions();
            options.position(pickup_lat_lng);
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.pickuploc));
            mMap.addMarker(options);
        /*    mMap.moveCamera(CameraUpdateFactory.newLatLng(pickup_lat_lng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));*/

            mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()), 10, new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                     mMap.moveCamera(CameraUpdateFactory.newLatLng(pickup_lat_lng));

                    //     startThread();
                }

                @Override
                public void onCancel() {

                }
            });


            //  mMap.setMaxZoomPreference(7f);
            //mMap.addMarker(new MarkerOptions().position(pickup_lat_lng));


            // End marker
            options = new MarkerOptions();
            options.position(drop_lat_lng);

            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.drop_locat));
            mMap.addMarker(options);

            //  mMap.animateCamera(CameraUpdateFactory.zoomTo(13f));

        }


    }

    private int calculateZoomLevel() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        double equatorLength = 40075004; // in meters
        double widthInPixels = width;
        double metersPerPixel = equatorLength / 256;
        int zoomLevel = 1;
        while ((metersPerPixel * widthInPixels) > 3000) {
            metersPerPixel /= 2;
            ++zoomLevel;
        }
        Log.i("ADNAN", "zoom level = " + zoomLevel);
        return zoomLevel;
    }


}
