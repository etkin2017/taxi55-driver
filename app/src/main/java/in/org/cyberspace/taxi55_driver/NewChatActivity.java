package in.org.cyberspace.taxi55_driver;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import in.org.cyberspace.taxi55_driver.application.Config;
import in.org.cyberspace.taxi55_driver.application.DAO;
import in.org.cyberspace.taxi55_driver.application.MyPreferenceManager;
import in.org.cyberspace.taxi55_driver.application.Taxi55;
import in.org.cyberspace.taxi55_driver.db.NotiDbHelper;
import in.org.cyberspace.taxi55_driver.model.NewMessage;
import in.org.cyberspace.taxi55_driver.services.NotificationReceiver;

public class NewChatActivity extends AppCompatActivity {

    private String TAG = NewChatActivity.class.getSimpleName();
    private RecyclerView recyclerView;
    private ChatRoomThreadAdapter mAdapter;
    private ArrayList<NewMessage> messageArrayList;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private EditText inputMessage;
    private Button btnSend;
    NotiDbHelper mHelper;
    SharedPreferences sh;
    int id;
    int chatRoomId, type;
    Map<String, String> data;
    int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    int REQUEST_TAKE_GALLERY_VIDEO = 100;
    Uri file_uri;
    ImageView back_arrow;

    Dialog a;
    private Dialog dialog1;
    String uname = "";
    public static boolean activate1 = true;
    Calendar c = Calendar.getInstance();
    SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
    String cdate = df.format(c.getTime());

    TextView chat_room_name;

    String block = "0";
    String rid = "", sid = "";
    MyNewBroadcast broadcast;
    String fcm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        setContentView(R.layout.activity_new_chat);
       /* Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);*/

        chat_room_name = (TextView) findViewById(R.id.chat_room_name);

        rid = getIntent().getStringExtra("rid");
        sid = getIntent().getStringExtra("sid");
        fcm = getIntent().getStringExtra("fcm");
        uname = getIntent().getStringExtra("uname");

        chat_room_name.setText(uname);


        if (fcm.equals("null")) {
            getFcm();
        }


        data = new HashMap<String, String>();
        mHelper = new NotiDbHelper(NewChatActivity.this);
        inputMessage = (EditText) findViewById(R.id.message);
        back_arrow = (ImageView) findViewById(R.id.back_arrow);
        btnSend = (Button) findViewById(R.id.btn_send);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mAdapter = new ChatRoomThreadAdapter(this, mHelper.getMessageByRid_Sid(sid, rid), uname);
        recyclerView.setLayoutManager(new LinearLayoutManager(NewChatActivity.this));
        recyclerView.setAdapter(mAdapter);


        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        try {

            if (mAdapter.getItemCount() > 1) {
                recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount());
            }

        } catch (Exception ee) {
            ee.printStackTrace();
        }
        broadcast = new MyNewBroadcast();
        IntentFilter filter = new IntentFilter(NotificationReceiver.name);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        try {
            registerReceiver(broadcast, filter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        activate1 = true;

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String message = inputMessage.getText().toString().trim();

                if (TextUtils.isEmpty(message)) {
                    Toast.makeText(getApplicationContext(), "Enter a message", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    sendMessage(message);
                }

            }
        });
    }

    public void getFcm() {
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("mobile_no", rid);
            jsonObject.put("type", "user");


            Log.i("data", jsonObject.toString());
        } catch (Exception ew) {
            ew.printStackTrace();
        }
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "get_fcm_from_phone", jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.i("Response", response.toString());
                        progressDialog.dismiss();
                        try {
                            if (response.getString("status").equals("success")) {

                                JSONArray jsonObject2 = response.getJSONArray("key");


                                fcm = jsonObject2.getJSONObject(0).getString("fcm");


                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(NewChatActivity.this, "User is offline", Toast.LENGTH_SHORT).show();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("Error", error.toString());
                error.printStackTrace();
                progressDialog.dismiss();
            }
        }
        );
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "ConfirmBookingRequest");
    }

    private void sendMessage(String message) {
        // NewMessage m;
        NewMessage m;
        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());

        DBHelper dbHelper = new DBHelper(NewChatActivity.this);
        HashMap<String, String> mobile_data = dbHelper.get_userData();

        m = new NewMessage(0, mobile_data.get("mobile"), rid, message, timeStamp + "", timeStamp + "", chatRoomId, 0, mobile_data.get("name"));
        //Log.e("LastMsg1", m.toString());

        mHelper.saveSingleMessage(m);
        inputMessage.setText("");
        refresh();
        // mAdapter.notifyDataSetChanged();

        //     mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());

        if (mAdapter.getItemCount() > 1) {
            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount());
        }

        mAdapter.notifyDataSetChanged();
        sendMsgToServer(m);


        // refresh();


    }

    @Override
    protected void onResume() {
        super.onResume();
        try {

            MyPreferenceManager myPreferenceManager = new MyPreferenceManager(NewChatActivity.this);

            // myPreferenceManager.clear();

            Log.e("RDI", rid);
            Taxi55.getInstance().getPrefManager().removeNotificaion(rid);

        } catch (Exception er1) {
            er1.printStackTrace();
        }
    }

    private void sendMsgToServer(NewMessage m) {

        final String msg1 = m.getMsg();
        final String sid1 = m.getSid() + "";
        final String rid1 = m.getRid() + "";
        //Log.e("RID", rid1);
        final String rtime1 = m.getRtime();
        String uid1 = m.getUid() + "";


        new AsyncTask() {
            JSONObject res = null;

            @Override
            protected Object doInBackground(Object[] params) {

                PrefManager my = new PrefManager(NewChatActivity.this);
                String un = my.getUsername();

                data.put("message", msg1);
                data.put("sid", sid1);
                data.put("rid", rid1);
                data.put("rtime", rtime1 + "");//rtime
                data.put("title", "Taxi55 - Driver App");
                data.put("chatroomid", "11");
                data.put("name", un);
                //data.put("gcm_reg_id", "cgr8lkH3NJo:APA91bE_GBr5VN2ozmIuRpsRxxlRZiDh_P6G6jRmil3h1jrfSmP45rHGH6WXkgpP9A17fdKQXjqTTnSZxmAcVqO-HLy_CcTgWBrZLFkkgaKYQjcvt95-Gno5vC9DLeO6eLvhMlaRMj8f");
                data.put("gcm_reg_id", fcm);
                res = DAO.getJsonFromUrl(Config.INSERT_MSG, data);


                return res;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                //   inputMessage.setText("");

                try {

                    Log.e("RES1", res.toString());
                    if (res.getInt("success") > 0) {
                        //       Toast.makeText(getApplicationContext(), "message sent", Toast.LENGTH_SHORT).show();

                    } else {
                        //     Toast.makeText(getApplicationContext(), "message failed", Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.execute(null, null, null);


    }


    private void refresh() {

        //   mAdapter.setMessageArrayList(mHelper.getMessageByRid_Sid(sid, rid));


        mAdapter = new ChatRoomThreadAdapter(this, mHelper.getMessageByRid_Sid(sid, rid), uname);
        recyclerView.setLayoutManager(new LinearLayoutManager(NewChatActivity.this));
        recyclerView.setAdapter(mAdapter);


        try {

            if (mAdapter.getItemCount() > 1) {
                recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() + 1);
            }

            mAdapter.notifyDataSetChanged();

        } catch (Exception ee) {
            ee.printStackTrace();
        }


        //   mAdapter.notifyDataSetChanged();

        //    mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());
      /*  if (mAdapter.getItemCount() > 1) {
            recyclerView.getLayoutManager().smoothScrollToPosition(recyclerView, null, mAdapter.getItemCount() + 1);
        }
        mAdapter.notifyDataSetChanged();*/

        //     mAdapter.notifyItemRangeChanged(0, mAdapter.getItemCount());


    }


    public class MyNewBroadcast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String bd = intent.getExtras().getString("data");
            refresh();
            //Log.e("badge_data", bd);
            Taxi55.getInstance().getPrefManager().removeNotificaion(rid);

            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                // Vibrate for 500 milliseconds

                v.vibrate(1000);
                r.play();
/*
                JSONObject j = new JSONObject(DAO.getNotification());
                JSONArray ar = new JSONArray();
                ar.put(bd.getString("msg"));
                ar.put(bd.getString("title"));
                ar.put(bd.getString("link"));
                ar.put(true);
                j.put(bd.getString("key"), ar);
                DAO.storeNotification(j.toString());*/


                //       Log.e("badge_data1", badge_data + "");


                // }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }
}
