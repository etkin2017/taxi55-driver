package in.org.cyberspace.taxi55_driver;

/**
 * Created by Wasim on 17/02/2017.
 */

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    ArrayList<String> id_com;
    ArrayList<String> from_com;
    ArrayList<String> to_com;
    ArrayList<String> amt_com;
    ArrayList<String> km_com;
    ArrayList<String> date_com;
    ArrayList<String> time_com;

    ArrayList<String> id_can;
    ArrayList<String> from_can;
    ArrayList<String> to_can;
    ArrayList<String> amt_can;
    ArrayList<String> km_can;
    ArrayList<String> date_can;
    ArrayList<String> time_can;
    public PagerAdapter(FragmentManager fm, int NumOfTabs,ArrayList<String> id_can, ArrayList<String> from_can, ArrayList<String> to_can, ArrayList<String> date_can, ArrayList<String> time_can, ArrayList<String> amt_can, ArrayList<String> km_can, ArrayList<String> id_com, ArrayList<String> from_com, ArrayList<String> to_com, ArrayList<String> date_com, ArrayList<String> time_com, ArrayList<String> amt_com, ArrayList<String> km_com) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
        this.id_can=id_can;
        this.id_com=id_com;

        this.from_can=from_can;
        this.from_com=from_com;

        this.to_can=to_can;
        this.to_com=to_com;

        this.amt_can=amt_can;
        this.amt_com=amt_com;

        this.km_can=km_can;
        this.km_com=km_com;

        this.date_can=date_can;
        this.date_com=date_com;

        this.time_can=time_can;
        this.time_com=time_com;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Bundle bundle= new Bundle();
                bundle.putStringArrayList("From",from_can);
                bundle.putStringArrayList("To",to_can);
                bundle.putStringArrayList("Date",date_can);
                bundle.putStringArrayList("Time",time_can);
                bundle.putStringArrayList("KM",km_can);
                bundle.putStringArrayList("ID",id_can);
                bundle.putStringArrayList("Amt",amt_can);
                FragmentBookingComplete tab1 = new FragmentBookingComplete();

                tab1.setArguments(bundle);
                return tab1;
            case 1:
                Bundle bundle2= new Bundle();
                bundle2.putStringArrayList("From",from_com);
                bundle2.putStringArrayList("To",to_com);
                bundle2.putStringArrayList("Date",date_com);
                bundle2.putStringArrayList("Time",time_com);
                bundle2.putStringArrayList("KM",km_com);
                bundle2.putStringArrayList("ID",id_com);
                bundle2.putStringArrayList("Amt",amt_com);
                FragmentBookingCancel tab2 = new FragmentBookingCancel();
                tab2.setArguments(bundle2);
                return tab2;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}