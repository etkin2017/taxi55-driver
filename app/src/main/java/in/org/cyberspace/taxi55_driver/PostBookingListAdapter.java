package in.org.cyberspace.taxi55_driver;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Wasim on 13/02/2017.
 */

public class PostBookingListAdapter extends BaseAdapter {

    ArrayList<String> ID, Dates, Times, Amt, KM, FROM, TO;
    Activity mContex;

    public PostBookingListAdapter(Activity context, ArrayList<String> id, ArrayList<String> dates, ArrayList<String> times, ArrayList<String> amt, ArrayList<String> km, ArrayList<String> from, ArrayList<String> to) {
        this.mContex = context;
        this.ID = id;
        this.Dates = dates;
        this.Times = times;
        this.Amt = amt;
        this.KM = km;
        this.FROM = from;
        this.TO = to;

        Log.i("Adapter AMT ", Amt.size() + "");
    }

    @Override
    public int getCount() {
        return ID.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        LayoutInflater layoutInflater = mContex.getLayoutInflater();
        View row = layoutInflater.inflate(R.layout.booking_list, viewGroup, false);
        TextView tvID, tvDate, tvTime, tvAmt, tvKM, tvFrom, tvTo, usnm;

        tvID = (TextView) row.findViewById(R.id.tvID);
        tvDate = (TextView) row.findViewById(R.id.tvDate);
        tvTime = (TextView) row.findViewById(R.id.tvTime);
        tvAmt = (TextView) row.findViewById(R.id.tvAmount);
        tvKM = (TextView) row.findViewById(R.id.tvDist);
        tvFrom = (TextView) row.findViewById(R.id.tvFrom);
        tvTo = (TextView) row.findViewById(R.id.tvTo);
        usnm = (TextView) row.findViewById(R.id.usnm);
        usnm.setVisibility(View.GONE);

        tvID.setText(ID.get(i));
        tvDate.setText("Date : " + Dates.get(i));
        tvTime.setText("Time : " + Times.get(i));
        tvAmt.setText("Amount : " + Amt.get(i));
        tvKM.setText("Distance : " + KM.get(i));
        String frm = FROM.get(i).replace("From", "").replace(":", "");
        tvFrom.setText( frm);//"From : " +
        String nm = TO.get(i).replace("To", "").replace(":", "");
        tvTo.setText(nm);//"To : " +


        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /*  String _ID = ID.get(i);
                Intent intent = new Intent(mContex, PostBookingDetail.class);
                intent.putExtra("ID", _ID);
                mContex.startActivity(intent);*/
            }
        });

        return row;

    }
}
