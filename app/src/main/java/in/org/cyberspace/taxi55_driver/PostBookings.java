package in.org.cyberspace.taxi55_driver;

import android.app.ProgressDialog;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class PostBookings extends AppCompatActivity {

    ArrayList<String> id_com;
    ArrayList<String> from_com;
    ArrayList<String> to_com;
    ArrayList<String> amt_com;
    ArrayList<String> km_com;
    ArrayList<String> date_com;
    ArrayList<String> time_com;

    ArrayList<String> id_can;
    ArrayList<String> from_can;
    ArrayList<String> to_can;
    ArrayList<String> amt_can;
    ArrayList<String> km_can;
    ArrayList<String> date_can;
    ArrayList<String> time_can;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_bookings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        id_com = new ArrayList<String>();
        from_com = new ArrayList<String>();
        to_com = new ArrayList<String>();
        amt_com = new ArrayList<String>();
        km_com = new ArrayList<String>();
        date_com = new ArrayList<String>();
        time_com = new ArrayList<String>();


        id_can = new ArrayList<String>();
        from_can = new ArrayList<String>();
        to_can = new ArrayList<String>();
        amt_can = new ArrayList<String>();
        km_can = new ArrayList<String>();
        date_can = new ArrayList<String>();
        time_can = new ArrayList<String>();

        fetchBookings();
    }


    public void fetchBookings()
    {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait...");
        dialog.show();
        PrefManager prefManager = new PrefManager(this);
        String Did = prefManager.getID();

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("driver_id",Did);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "driver_bookings", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                dialog.dismiss();
                Log.i("Response",response.toString());
                try {
                    JSONArray jsonArray = response.getJSONArray("new_rade");



                    for (int i = 0 ;i<jsonArray.length();i++)
                    {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        id_can.add(jsonObject1.getString("uneaque_id"));
                        from_can.add(jsonObject1.getString("pickup_area"));
                        to_can.add(jsonObject1.getString("drop_area"));
                        date_can.add(jsonObject1.getString("pickup_date"));
                        time_can.add(jsonObject1.getString("pickup_time"));
                        amt_can.add(jsonObject1.getString("amount"));
                        km_can.add(jsonObject1.getString("km"));
                    }


                    JSONArray jsonArray2 = response.getJSONArray("all");



                    for (int i = 0 ;i<jsonArray2.length();i++)
                    {
                        JSONObject jsonObject1 = jsonArray2.getJSONObject(i);
                        id_com.add(jsonObject1.getString("uneaque_id"));
                        from_com.add(jsonObject1.getString("pickup_area"));
                        to_com.add(jsonObject1.getString("drop_area"));
                        date_com.add(jsonObject1.getString("pickup_date"));
                        time_com.add(jsonObject1.getString("pickup_time"));
                        amt_com.add(jsonObject1.getString("amount"));
                        km_com.add(jsonObject1.getString("km"));
                    }

                    displayTabs();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
                error.printStackTrace();
            }
        });

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest,"BookingRequest");
    }


    public  void displayTabs()
    {
        Log.i("Display Can: ",amt_can.size()+"");
        Log.i("Display Com : ",amt_com.size()+"");

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Completed"));
        tabLayout.addTab(tabLayout.newTab().setText("Cancel"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), id_can,from_can,to_can,date_can,time_can,amt_can,km_can,id_com,from_com,to_com,date_com,time_com,amt_com,km_com);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}
