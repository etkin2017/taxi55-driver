package in.org.cyberspace.taxi55_driver;

/**
 * Created by Wasim on 13/01/2017.
 */
import android.content.Context;
import android.content.SharedPreferences;

public class PrefManager {
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "TAXI55";

    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String IS_USER_LOGGED_IN = "IsUserLoggedIn";
    private static final String USER_NAME = "UserName";
    private static final String USER_ID = "UserID";

    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public void setIsUserLoggedIn(boolean isUserLoggedIn) {
        editor.putBoolean(IS_USER_LOGGED_IN, isUserLoggedIn);
        editor.commit();
    }

    public void setUserName(String username) {
        editor.putString(USER_NAME, username);
        editor.commit();
    }

    public void setUserID(String id) {
        editor.putString(USER_ID, id);
        editor.commit();
    }



    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
    public boolean isUserLoggedIn () {return pref.getBoolean(IS_USER_LOGGED_IN, false);}
    public String getUsername () {return pref.getString(USER_NAME, "");}
    public String getID () {return pref.getString(USER_ID, "");}
    }

