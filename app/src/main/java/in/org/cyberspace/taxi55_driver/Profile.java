package in.org.cyberspace.taxi55_driver;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import in.org.cyberspace.taxi55_driver.model.CarModel;
import in.org.cyberspace.taxi55_driver.model.PlaceArrayAdapter;
import in.org.cyberspace.taxi55_driver.model.Subscription;

public class Profile extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {

    final String[] TYPE = {"Day", "Night"};
    Spinner cat_type_spinner, time_type_spinner;
    String LOG_TAG = "Profile.Class";
    ImageView back_arrow;
    EditText txtName, txtEmail, txtPhone;
    TextView tvUserName;
    AutoCompleteTextView autoCompleteTextView_from_loc;
    private GoogleApiClient mGoogleApiClient;
    private PlaceArrayAdapter mPlaceArrayAdapter;
    private static final LatLngBounds BOUNDS_MOUNTAIN_VIEW = new LatLngBounds(new LatLng(37.398160, -122.180831), new LatLng(37.430610, -121.972090));

    String mlatlng;
    String parts[];
    String part1, part2;
    String curlocation = "", src1 = "", srclat, srclng;
    private static final int GOOGLE_API_CLIENT_ID = 0;
    Button button3;

    String car_type;
    ArrayList<CarModel> car_list;

    public void init() {
        tvUserName = (TextView) findViewById(R.id.tvUsername);
        txtName = (EditText) findViewById(R.id.txtName);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPhone = (EditText) findViewById(R.id.txtPhone);
        time_type_spinner = (Spinner) findViewById(R.id.time_type_spinner);
        cat_type_spinner = (Spinner) findViewById(R.id.cat_type_spinner);

        button3 = (Button) findViewById(R.id.button3);
        car_list = new ArrayList<>();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        init();
        back_arrow = (ImageView) findViewById(R.id.back_arrow);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                startActivity(new Intent(Profile.this, Home.class));

                // onBackPressed();
            }
        });
        PrefManager prefManager = new PrefManager(this);
        tvUserName.setText("Username : " + prefManager.getUsername());
        DBHelper dbHelper = new DBHelper(Profile.this);
        HashMap<String, String> data = dbHelper.get_userData();

        getCarsType();

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(autoCompleteTextView_from_loc.getText().toString())) {
                    autoCompleteTextView_from_loc.setError("Enter Preferred Location");
                } else {
                    updateDriverProfile();
                }
            }
        });


        cat_type_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cat_type_spinner.setSelection(position);
                car_type = car_list.get(position).getCar_type();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });



     /*   menu_profile_name.setText(userData.get("name") + "");
        menu_profile_emailid.setText(userData.get("email") + "");*/

        txtEmail.setText("Email Id : " + data.get("email"));
        txtName.setText("Name : " + data.get("name"));
        txtPhone.setText("Phone : " + data.get("mobile"));


        try {
            mGoogleApiClient = new GoogleApiClient.Builder(getApplicationContext()).enableAutoManage(this, GOOGLE_API_CLIENT_ID, this)
                    .addApi(Places.GEO_DATA_API).addConnectionCallbacks(this).build();
        } catch (Exception e) {
            e.printStackTrace();
        }


        autoCompleteTextView_from_loc = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView_from_loc);


        autoCompleteTextView_from_loc.setThreshold(3);
        autoCompleteTextView_from_loc.setOnItemClickListener(mAutocompleteClickListenerSource);
        mPlaceArrayAdapter = new PlaceArrayAdapter(getApplicationContext(), R.layout.item_spinner, BOUNDS_MOUNTAIN_VIEW, null);
        autoCompleteTextView_from_loc.setAdapter(mPlaceArrayAdapter);


        /*spinner = (Spinner) findViewById(R.id.spinner);

*/
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.spinner_layout2, TYPE);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        time_type_spinner.setAdapter(arrayAdapter);


    }

    private AdapterView.OnItemClickListener mAutocompleteClickListenerSource = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final PlaceArrayAdapter.PlaceAutocomplete item = mPlaceArrayAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);
            curlocation = String.valueOf(item.main_desc);
            Log.i(LOG_TAG, "Selected: " + item.main_desc);
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallbackSource);
            Log.i(LOG_TAG, "Fetching details for ID: " + item.placeId);
            src1 = curlocation;



           /* Intent i=new Intent(HomeActivity.this,FindRideFragment.class);
            i.putExtra("SRC",src);*/

        }

    };


    public void updateDriverProfile() {
        final ProgressDialog pdlg = new ProgressDialog(Profile.this);
        pdlg.setMessage("Please Wait...");
        pdlg.setCancelable(false);
        pdlg.show();

        PrefManager prefManager = new PrefManager(Profile.this);
        String driver_id = prefManager.getID();

        String REQUEST_TAG = getPackageName() + ".update_driver_profile";
        final JSONObject jsonObject = new JSONObject();
        try {


//car_type,timetype,preffered_location,latitude,longitude
            jsonObject.put("car_type", car_type);
            jsonObject.put("timetype", time_type_spinner.getSelectedItem().toString());
            jsonObject.put("preffered_location", curlocation);
            jsonObject.put("latitude", srclat);
            jsonObject.put("longitude", srclng);
            jsonObject.put("driver_id", driver_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "update_driver_profile", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Response", response.toString());
                pdlg.dismiss();
                try {
                    //JSONObject jsonObject1 = response.ge(0);
                    if (response.getString("status").equals("success")) {
                        finish();
                        startActivity(new Intent(Profile.this, Home.class));
                    } else {
                        Toast.makeText(Profile.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (JSONException e) {
                    Toast.makeText(Profile.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        pdlg.dismiss();
                        error.printStackTrace();
                        Log.i("Response", error.toString());
                    }
                });
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonArrayRequest, REQUEST_TAG);

    }

    public void getCarsType() {

        final ArrayList<CarModel> list1 = new ArrayList<>();

        String REQUEST_TAG = getPackageName() + ".getSubPlans";
        final JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("Email", "abc");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url1 = ConnectionConfiguration.URL + "fetch_car_types";
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(url1, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Response", response.toString());

                try {
                    // JSONObject jsonObject1 = response.getJSONObject(0);
                    if (response.getString("status").equals("success")) {


                        try {
                            JSONArray res1 = response.getJSONArray("data");


                            for (int i = 0; i < res1.length(); i++) {

                                CarModel carModel = new CarModel(res1.getJSONObject(i).getString("id"),
                                        res1.getJSONObject(i).getString("car_type")
                                        , res1.getJSONObject(i).getString("car_image"));

                                list1.add(carModel);
                                car_list.add(carModel);
                            }

                           /* ArrayList<String> list = new ArrayList<String>();
                            if (list.isEmpty()) {
                                for (int i = 0; i < car_list.size(); i++) {
                                    list.add(car_list.get(i).getCar_img_nm());
                                }
                            }*/

                            //ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, list);
                            //dataAdapter.setDropDownViewResource(R.layout.item_spinner);

                            CustomSpinnerAdapter dataAdapter = new CustomSpinnerAdapter(getApplicationContext(), list1);

                            cat_type_spinner.setAdapter(dataAdapter);


                            try {
                                cat_type_spinner.setSelection(list1.indexOf(0));

                                int a = getIndex(car_list.get(0).getCar_type());
                                CarModel ii = car_list.get(a);
                                cat_type_spinner.setSelection(a);

                                car_type = car_list.get(0).getCar_type();


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }


                    } else {
                        Toast.makeText(Profile.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (JSONException e) {
                    Toast.makeText(Profile.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();
                        Log.i("Response", error.toString());
                    }
                });
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonArrayRequest, REQUEST_TAG);


    }


    public int getIndex(String itemName) {
        for (int i = 0; i < car_list.size(); i++) {
            CarModel auction = car_list.get(i);
            if (itemName.equals(auction.getCar_type())) {
                return i;
            }
        }

        return -1;
    }


    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallbackSource = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                Log.e(LOG_TAG, "Place query did not complete. Error: " + places.getStatus().toString());
                return;
            }
            // Selecting the first object buffer.
            final Place place = places.get(0);
            CharSequence attributions = places.getAttributions();

            String locnm = Html.fromHtml(place.getAddress() + "") + "";
            Log.e("locnm", locnm);
            mlatlng = Html.fromHtml(place.getLatLng() + "") + "";//mLatLngTextView.getText().toString();

            parts = mlatlng.split(":");
            part1 = parts[0];
            part2 = parts[1];

            String[] str1 = part2.split(",");
            String s1 = str1[0];
            String s2 = str1[1];

            String[] s3 = s1.split("\\(");
            srclat = s3[1];

            String[] s4 = s2.split("\\)");
            srclng = s4[0];

            autoCompleteTextView_from_loc.setText(curlocation);

          /*  DbHandler dbHandler = new DbHandler(getActivity());
            dbHandler.addPlace(curlocation, srclat, srclng);
            callMethod(curlocation);*/
            //  Toast.makeText(getActivity(), srclat + "," + srclng, Toast.LENGTH_LONG).show();


        }
    };

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
            mGoogleApiClient.stopAutoManage(this);
        }
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
      /*  try {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }catch (Exception ex1){
            ex1.printStackTrace();
        }*/

        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(this);
            mGoogleApiClient.disconnect();
        }

    }

    @Override
    public void onPause() {
        super.onPause();

        mGoogleApiClient.stopAutoManage(this);
        mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mPlaceArrayAdapter.setGoogleApiClient(mGoogleApiClient);
        Log.i(LOG_TAG, "Google Places API connected.");

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e(LOG_TAG, "Google Places API connection failed with error code: "
                + connectionResult.getErrorCode());

        Toast.makeText(getApplicationContext(),
                "Google Places API connection failed with error code:" +
                        connectionResult.getErrorCode(),
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mPlaceArrayAdapter.setGoogleApiClient(null);
        Log.e(LOG_TAG, "Google Places API connection suspended.");
    }

    public class CustomSpinnerAdapter extends BaseAdapter {
        Context context;
        int flags[];
        String[] countryNames;
        LayoutInflater inflter;

        //Context context;
        ArrayList<CarModel> list;

        public CustomSpinnerAdapter(Context applicationContext, ArrayList<CarModel> list) {
            this.context = applicationContext;
            this.list = list;

        }

        public CustomSpinnerAdapter() {

        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            // view = inflter.inflate(R.layout.activity_spinner_for_std_code, null);

            view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.car_type_row, viewGroup, false);
    /*    ImageView icon = (ImageView) view.findViewById(R.id.imageView);
        TextView names = (TextView) view.findViewById(R.id.textView);
        icon.setImageResource(flags[i]);
        names.setText(countryNames[i]);*/


            CircleImageView imageView = (CircleImageView) view.findViewById(R.id.car_img);

            String url = list.get(i).getCar_img_nm();
            Log.e("Url" + i, url);

            imageView.setImageURI(Uri.parse(url));
            TextView textView = (TextView) view.findViewById(R.id.car_nm);
            TextView txtId = (TextView) view.findViewById(R.id.car_id);
            textView.setText(list.get(i).getCar_type());
            txtId.setText(list.get(i).getCar_id());
            return view;
        }
    }

}
