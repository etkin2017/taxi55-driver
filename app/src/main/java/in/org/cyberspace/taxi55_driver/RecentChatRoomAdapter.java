package in.org.cyberspace.taxi55_driver;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import in.org.cyberspace.taxi55_driver.application.Taxi55;
import in.org.cyberspace.taxi55_driver.model.User;


/**
 * Created by Ashvini on 3/8/2017.
 */

public class RecentChatRoomAdapter extends RecyclerView.Adapter<RecentChatRoomAdapter.ViewHolder> {


    List<User> userList;
    Context mCtx;

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public RecentChatRoomAdapter(List<User> userList, Context mCtx) {
        this.userList = userList;
        this.mCtx = mCtx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        //  View view = LayoutInflater.from(mCtx).inflate(R.layout.chat_row, parent);
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.chat_row, parent, false);

        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.name.setText(userList.get(position).getName());//;
        holder.contact.setText(userList.get(position).getContact());


        holder.chat_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                DBHelper dbHelper = new DBHelper(mCtx);
                HashMap<String, String> mobile_data = dbHelper.get_userData();
                Intent new_int = new Intent(mCtx, NewChatActivity.class);
                new_int.putExtra("rid", holder.contact.getText().toString());
                new_int.putExtra("sid", mobile_data.get("mobile"));
                new_int.putExtra("fcm", "null");
                new_int.putExtra("uname", holder.name.getText().toString());
                mCtx.startActivity(new_int);

            }
        });

        try {

            String a = "";
            int b = 0;

            String id = holder.contact.getText().toString();

            String oldNotification = Taxi55.getInstance().getPrefManager().getNotificationsById(id);
            List<String> messages = Arrays.asList(oldNotification.split("\\|"));

            Set<String> unique = new HashSet<String>(messages);
            for (String key : unique) {
                System.out.println(key + ": " + Collections.frequency(messages, key));
                a = key;
                b = Collections.frequency(messages, key);

            }

            if (holder.contact.getText().toString().equals(a)) {
                holder.count.setText(b + "");
                holder.count.setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, contact, count;
        LinearLayout chat_container;

        public ViewHolder(View itemView) {
            super(itemView);

            name = (TextView) itemView.findViewById(R.id.chat_name);
            contact = (TextView) itemView.findViewById(R.id.chat_cont);
            count = (TextView) itemView.findViewById(R.id.count);

            chat_container = (LinearLayout) itemView.findViewById(R.id.chat_container);

        }
    }

}
