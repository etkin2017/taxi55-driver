package in.org.cyberspace.taxi55_driver;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.AppBarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.org.cyberspace.taxi55_driver.model.Subscription;

public class RenewActivity extends AppCompatActivity {

    String pnm;
    String pid;
    List<Subscription> sub_plan;
    String plan_id = "";
    Spinner add_sub_scription;
    EditText txtUser;// = (EditText)findViewById(R.id.input_username);
    EditText txtEmail;//  = (EditText)findViewById(R.id.input_email);
    EditText txtMobile;//  = (EditText)findViewById(R.id.input_mobile);
    EditText txtPass;//  = (EditText)findViewById(R.id.input_password);
    EditText txtCPass;
    ;// = (EditText)findViewById(R.id.input_c_passworde);
    EditText txtName;// = (EditText)findViewById(R.id.input_name);
    AppBarLayout appBarLayout;
    ImageView back_arrow;
    TextView sign_txt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_layout);
        Bundle bb = getIntent().getExtras();
        pid = bb.getString("plan_id");
        pnm = bb.getString("plan_nm");
        sub_plan = new ArrayList<Subscription>();

        appBarLayout = (AppBarLayout) findViewById(R.id.appBar_lay);
        appBarLayout.setVisibility(View.VISIBLE);
        back_arrow = (ImageView) findViewById(R.id.back_arrow);
        sign_txt = (TextView) findViewById(R.id.sign_txt);
        sign_txt.setText("Renew Subscription");
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        getSupscription();


        txtUser = (EditText) findViewById(R.id.input_username);
        txtEmail = (EditText) findViewById(R.id.input_email);
        txtMobile = (EditText) findViewById(R.id.input_mobile);
        txtPass = (EditText) findViewById(R.id.input_password);
        txtCPass = (EditText) findViewById(R.id.input_c_passworde);
        txtName = (EditText) findViewById(R.id.input_name);

        DBHelper db = new DBHelper(getApplicationContext());
        HashMap<String, String> userData = db.get_userData();
        txtName.setText(userData.get("name") + "");
        txtEmail.setText(userData.get("email") + "");
        txtMobile.setText(userData.get("mobile") + "");
        txtUser.setText(userData.get("username") + "");

        Button dialogButton = (Button) findViewById(R.id.singup_dialog_btn);


        add_sub_scription = (Spinner) findViewById(R.id.add_sub_scription);


        List<String> list = new ArrayList<String>();
        if (list.isEmpty()) {
            for (int i = 0; i < sub_plan.size(); i++) {
                list.add(sub_plan.get(i).getName());
            }
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, list);
        //dataAdapter.setDropDownViewResource(R.layout.item_spinner);
        add_sub_scription.setAdapter(dataAdapter);

        try {
            add_sub_scription.setSelection(sub_plan.indexOf(0));

            int a = getIndex(pnm);
            Subscription ii = sub_plan.get(a);
            add_sub_scription.setSelection(a);

            plan_id = sub_plan.get(0).getId();

        } catch (Exception e) {
            e.printStackTrace();
        }

        add_sub_scription.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                add_sub_scription.setSelection(position);
                plan_id = sub_plan.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final String username = txtUser.getText().toString().trim();
                final String name = txtName.getText().toString().trim();
                final String mobile = txtMobile.getText().toString().trim();
                final String email = txtEmail.getText().toString().trim();
                final String pass = txtPass.getText().toString().trim();
                String cPass = txtCPass.getText().toString().trim();

                if (name.equals("")) {
                    txtName.setError("Enter Your Name !");
                    txtName.requestFocus();
                    return;
                }

                if (username.equals("")) {
                    txtUser.setError("Enter Username !");
                    txtUser.requestFocus();
                    return;
                }

                if (email.equals("")) {
                    txtEmail.setError("Enter Email !");
                    txtEmail.requestFocus();
                    return;
                }

                if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    txtEmail.setError("Enter Valid Email !");
                    txtEmail.requestFocus();
                    return;
                }

                if (mobile.equals("")) {
                    txtMobile.setError("Enter Mobile Number !");
                    txtMobile.requestFocus();
                    return;
                }

                if (pass.equals("")) {
                    txtPass.setError("Enter Password !");
                    txtPass.requestFocus();
                    return;
                }

                if (cPass.equals("")) {
                    txtCPass.setError("Confirmn Password !");
                    txtCPass.requestFocus();
                    return;
                }

                if (!pass.equals(cPass)) {
                    txtCPass.setError("Confirmn Password Did Not Match !");
                    txtCPass.requestFocus();
                    return;
                }
                final ProgressDialog pdlg = new ProgressDialog(RenewActivity.this);
                pdlg.setMessage("Please Wait...");
                pdlg.show();
                pdlg.setCancelable(false);
                String REQUEST_TAG = getPackageName() + ".volleyJsonArrayRequest";
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("secret_key", ConnectionConfiguration.SECRET_KEY);
                    jsonObject.put("Email", email);
                    jsonObject.put("Password", pass);
                    jsonObject.put("User_name", username);
                    jsonObject.put("Mobile", mobile);
                    jsonObject.put("Name", name);
                    jsonObject.put("sub_plan_id", plan_id);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

             /*   JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "driver_sign_up", jsonObject, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("Response", response.toString());
                        pdlg.dismiss();
                        try {
                            if (response.getString("status").equals("success")) {
                                DBHelper db = new DBHelper(RenewActivity.this);
                                db.saveUserData(username, name, pass, email, mobile, response.getString("id"));
                                PrefManager prefManager = new PrefManager(RenewActivity.this);
                                prefManager.setIsUserLoggedIn(true);
                                prefManager.setUserName(username);
                                prefManager.setUserID(response.getString("id"));
                                startActivity(new Intent(RenewActivity.this, Home.class));
                                finish();
                            } else {
                                Toast.makeText(RenewActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace();
                                Log.i("Response", error.toString());
                                pdlg.dismiss();
                                Toast.makeText(RenewActivity.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                            }
                        });
                jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                        5000,
                        DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

                AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, REQUEST_TAG);
*/

            }
        });

    }

    public void getSupscription() {


        String REQUEST_TAG = getPackageName() + ".getSubPlans";
        final JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("Email", "abc");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url1 = ConnectionConfiguration.URL+"fetch_sub_plans";
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(url1, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Response", response.toString());

                try {
                    // JSONObject jsonObject1 = response.getJSONObject(0);
                    if (response.getString("status").equals("success")) {


                        try {
                            JSONArray res1 = response.getJSONArray("data");


                            for (int i = 0; i < res1.length(); i++) {

                                Subscription subscription = new Subscription(res1.getJSONObject(i).getString("id"),
                                        res1.getJSONObject(i).getString("price")
                                        , res1.getJSONObject(i).getString("name"));

                                sub_plan.add(subscription);
                            }


                            List<String> list = new ArrayList<String>();
                            if (list.isEmpty()) {
                                for (int i = 0; i < sub_plan.size(); i++) {
                                    list.add(sub_plan.get(i).getName());
                                }
                            }

                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, list);
                            //dataAdapter.setDropDownViewResource(R.layout.item_spinner);
                            add_sub_scription.setAdapter(dataAdapter);

                            try {
                                add_sub_scription.setSelection(sub_plan.indexOf(0));

                                int a = getIndex(pnm);
                                Subscription ii = sub_plan.get(a);
                                add_sub_scription.setSelection(a);

                                plan_id = sub_plan.get(0).getId();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }


                    } else {
                        Toast.makeText(RenewActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (JSONException e) {
                    Toast.makeText(RenewActivity.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        error.printStackTrace();
                        Log.i("Response", error.toString());
                    }
                });
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonArrayRequest, REQUEST_TAG);


    }

    public int getIndex(String itemName) {
        for (int i = 0; i < sub_plan.size(); i++) {
            Subscription auction = sub_plan.get(i);
            if (itemName.equals(auction.getName())) {
                return i;
            }
        }

        return -1;
    }
}
