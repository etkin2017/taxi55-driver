package in.org.cyberspace.taxi55_driver;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.org.cyberspace.taxi55_driver.model.GPSTracker;

public class ShowRequestFromUserActivity extends AppCompatActivity implements OnMapReadyCallback {


    Button accept_noti, cancel_noti;
    TextView drop_location, time_reach;
    double lat = 0.0, lng = 0.0, drop_lat = 0.0, drop_lng = 0.0;
    double my_lat = 0.0, my_lng = 0.0;
    GoogleMap mMap;
    String booking_id = "";
    LatLng PICK, DROP;
    String DIST = "";
    String DURATION = "";
    TextView txt_amt, txt_time, txt_dist;
    GPSTracker gps;


    public void init() {
        accept_noti = (Button) findViewById(R.id.accept_noti);
        cancel_noti = (Button) findViewById(R.id.cancel_noti);
        drop_location = (TextView) findViewById(R.id.drop_location);
        time_reach = (TextView) findViewById(R.id.time_reach);
        txt_amt = (TextView) findViewById(R.id.txt_amt);
        txt_time = (TextView) findViewById(R.id.txt_time);
        txt_dist = (TextView) findViewById(R.id.txt_dist);

        gps = new GPSTracker(ShowRequestFromUserActivity.this);
        ;
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_request_from_user);
        init();

        try {

            String message = getIntent().getStringExtra("message");
            booking_id = getIntent().getStringExtra("booking_id");

            lat = Double.parseDouble(getIntent().getStringExtra("lat"));
            lng = Double.parseDouble(getIntent().getStringExtra("lng"));
            PICK = new LatLng(lat, lng);


            drop_lat = Double.parseDouble(getIntent().getStringExtra("drop_lat"));
            drop_lng = Double.parseDouble(getIntent().getStringExtra("drop_lng"));

            DROP = new LatLng(drop_lat, drop_lng);

            drop_location.setText(message);

            String name1 = getIntent().getStringExtra("name1");
            String time = getIntent().getStringExtra("time");
            String dist = getIntent().getStringExtra("dist");
            String amt = getIntent().getStringExtra("amount");


            time_reach.setText(name1);
            txt_amt.setText(amt + "");
            txt_dist.setText(dist + "");
            txt_time.setText(time + "");


        } catch (Exception ew) {
            ew.printStackTrace();
        }

        accept_noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder dlg = new AlertDialog.Builder(ShowRequestFromUserActivity.this);
                dlg.setTitle(R.string.app_name);
                dlg.setMessage("Are You Sure You Want To accept this ride ?");
                dlg.setIcon(R.drawable.icon);
                dlg.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                        acceptBooking(booking_id);

                    }
                });


                dlg.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        return;
                    }
                });

                dlg.create().show();


            }

        });

        cancel_noti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();

            }
        });

    }

    /*public void acceptBooking(){

        String url ="http://uncletaxi.sarodestudio.photography/web_service/driver_accept_request";

    }
*/

    public void acceptBooking(final String booking_id) {

        String url = ConnectionConfiguration.URL+"driver_accept_request";
        JSONObject jsonObject = new JSONObject();
        PrefManager prefManager = new PrefManager(this);

        String driver_id = prefManager.getID();
        try {
            jsonObject.put("booking_id", booking_id);
            // jsonObject.put("booking_status", "Processing");
            jsonObject.put("driver_id", driver_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.i("data", jsonObject.toString());
        final ProgressDialog pdlg = new ProgressDialog(ShowRequestFromUserActivity.this);
        pdlg.setMessage("Please Wait...");
        pdlg.show();
        pdlg.setCancelable(false);
        String REQUEST_TAG = getPackageName() + ".volleyJsonArrayRequest";

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Response", response.toString());
                pdlg.dismiss();
                try {
                    if (response.getString("status").equals("success")) {
                     /*   JSONArray jsonArray = response.getJSONArray("driver_details");
                        final String[] FCMS = new String[jsonArray.length()];

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject cab = jsonArray.getJSONObject(i);
                            FCMS[i] = cab.getString("fcm");


                        }*/

                        PrefManager prefManager1 = new PrefManager(getApplicationContext());
                        boolean ch = prefManager1.isUserLoggedIn();


                        if (ch == true) {
                            // startActivity(new Intent(ShowRequestFromUserActivity.this, Home.class));
                            pdlg.dismiss();
                            go_to_map(booking_id);
                        } else {
                            startActivity(new Intent(ShowRequestFromUserActivity.this, Splash.class));

                        }
                        Toast.makeText(ShowRequestFromUserActivity.this, "booking confirmed", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ShowRequestFromUserActivity.this, response.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                        Log.i("Response", error.toString());
                        pdlg.dismiss();
                        Toast.makeText(ShowRequestFromUserActivity.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                    }
                });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                10000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, REQUEST_TAG);

    }


    public void go_to_map(String booking_id) {
        final ProgressDialog dialog = new ProgressDialog(ShowRequestFromUserActivity.this);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait...");
       // dialog.show();
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("booking_id", booking_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "get_accept_booking_detail", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
             //   dialog.dismiss();
                Log.e("PassengerResponse", response.toString());
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    Intent mapAct = new Intent(ShowRequestFromUserActivity.this, MapActivity.class);
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    Bundle b = new Bundle();
                    b.putString("date", jsonObject1.getString("pickup_date"));
                    b.putString("id", jsonObject1.getString("uneaque_id"));
                    b.putString("time", jsonObject1.getString("pickup_time"));
                    b.putString("amt", jsonObject1.getString("amount"));
                    b.putString("km", jsonObject1.getString("distance"));
                    b.putString("from", jsonObject1.getString("pickup_area"));
                    b.putString("to", jsonObject1.getString("drop_area"));
                    b.putString("uname", jsonObject1.getString("username"));
                    b.putString("phone", jsonObject1.getString("mobile"));
                    b.putString("fcm", jsonObject1.getString("fcm"));
                    b.putString("user_id",jsonObject1.getString("id"));
                    mapAct.putExtras(b);
                    startActivity(mapAct);


                } catch (JSONException e) {
                 //   dialog.dismiss();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
             //   dialog.dismiss();
                error.printStackTrace();
            }
        }

        );

        jsonObjectRequest.setRetryPolicy(new

                DefaultRetryPolicy(5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT)

        );
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


      /*  mMap.addMarker(new MarkerOptions().position(new LatLng(lat, lng)).icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_plain)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(lat, lng)));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(10));
        mMap.setMaxZoomPreference(13f);*/


        new AsyncTask() {
            @Override
            protected Object doInBackground(Object[] params) {

                try {

                    if (gps.canGetLocation()) {
                        my_lat = gps.getLatitude();
                        my_lng = gps.getLongitude();

                        LatLng MyLOc = new LatLng(my_lat, my_lng);
                        String url1 = getDirectionsUrl(MyLOc, PICK);

                        DownloadTask downloadTask = new DownloadTask();

                        // Start downloading json data from Google Directions API
                        downloadTask.execute(url1);

                        Marker m = mMap.addMarker(new MarkerOptions().position(new LatLng(my_lat, my_lng))
                                .title("This is you")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.blue_pin3))
                                .draggable(true));
                        m.showInfoWindow();

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);

                if (PICK != null && DROP != null) {
                    // Getting URL to the Google Directions API
                    String url = getDirectionsUrl(PICK, DROP);

                    DownloadTask downloadTask = new DownloadTask();

                    // Start downloading json data from Google Directions API
                    downloadTask.execute(url);

                    Marker pick_up_m = mMap.addMarker(new MarkerOptions().position(PICK)
                            .title("Pickup Point")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.pink_pin3))
                            .draggable(true)
                    );
                    Marker drop_marker_m = mMap.addMarker(new MarkerOptions().position(DROP)
                            .title("Drop-Off Point")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.grren_pin3))
                            .draggable(true));
                    pick_up_m.showInfoWindow();
                    drop_marker_m.showInfoWindow();

                    LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
                    boundsBuilder.include(PICK);
                    boundsBuilder.include(DROP);

                    LatLngBounds bounds = boundsBuilder.build();
                    mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 3));
                }

            }
        }.execute();





    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {

        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;


        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;


        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Error", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    // Fetches data from url passed
    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread
        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            ParserTask parserTask = new ParserTask();

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result);

        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

      /*  // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                               // Starts parsing data
                routes = parser.parse(jObject);
            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            // Traversing through all the routes
            for(int i=0;i<result.size();i++){
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for(int j=0;j<path.size();j++){
                    HashMap<String,String> point = path.get(j);

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    if(j==0){    // Get distance from the list
                        DIST = (String)point.get("distance");
                        continue;
                    }else if(j==1){ // Get duration from the list
                        DURATION = (String)point.get("duration");
                        continue;
                    }

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.RED);

            }

            Toast.makeText(Home.this,"Dist "+DIST +"Duration  "+DURATION,Toast.LENGTH_SHORT).show();
            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }
        */


        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            if (result.size() < 1) {
                Toast.makeText(getBaseContext(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {    // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duration = (String) point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.parseColor("#399ed7"));
            }

            DIST = distance;
            DURATION = duration;
            // Toast.makeText(Home.this,"Dist "+distance +"Duration  "+duration,Toast.LENGTH_SHORT).show();
            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }
    }
}
