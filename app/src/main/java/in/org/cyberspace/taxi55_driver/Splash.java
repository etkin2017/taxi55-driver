package in.org.cyberspace.taxi55_driver;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

public class Splash extends AppCompatActivity {
    PrefManager prefManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ImageView imageView=(ImageView) findViewById(R.id.imageView);
        Animation animation = AnimationUtils.loadAnimation(this,R.anim.fade_in);
        imageView.setAnimation(animation);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {


                prefManager = new PrefManager(Splash.this);

                    if(prefManager.isUserLoggedIn()) {
                        Intent i = new Intent(Splash.this, Home.class);
                        startActivity(i);
                        finish();
                        Log.i("Class","Main");
                    }
                    else
                    {
                        Intent i = new Intent(Splash.this, Login.class);
                        startActivity(i);
                        finish();
                        Log.i("Class","Login");
                    }

            }
        },5000);

    }
}
