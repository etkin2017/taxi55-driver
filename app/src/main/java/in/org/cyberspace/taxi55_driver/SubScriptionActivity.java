package in.org.cyberspace.taxi55_driver;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;


import in.org.cyberspace.taxi55_driver.model.Subscription;

public class SubScriptionActivity extends AppCompatActivity {

    DBHelper db;
    List<Subscription> sub_plan;
    TextView plan_id, plan_nm, from_date, to_date, status, price;
    ImageView back_arrow;
    private static final DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
    TextView hrs, days, month;
    Button renew;

    public void init() {
        plan_id = (TextView) findViewById(R.id.plan_id);
        plan_nm = (TextView) findViewById(R.id.plan_nm);
        from_date = (TextView) findViewById(R.id.from_date);
        to_date = (TextView) findViewById(R.id.to_date);
        status = (TextView) findViewById(R.id.status);
        price = (TextView) findViewById(R.id.price);
        back_arrow = (ImageView) findViewById(R.id.back_arrow);

        hrs = (TextView) findViewById(R.id.hrs);
        days = (TextView) findViewById(R.id.days);
        month = (TextView) findViewById(R.id.month);
        renew = (Button) findViewById(R.id.renew);
        renew.setEnabled(false);

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sub_scription);
        init();

        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        sub_plan = new ArrayList<>();
        fetchBookings();
    }


    public void getSupscription(final Spinner add_sub_scription, final Button renew_ok, final Dialog dialog11) {
        final String[] plan_id1 = {""};

        final ProgressDialog pdlg = new ProgressDialog(SubScriptionActivity.this);
        pdlg.setMessage("Please Wait...");
        pdlg.show();
        pdlg.setCancelable(false);
        String REQUEST_TAG = getPackageName() + ".getSubPlans";
        final JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("Email", "abc");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url1 = ConnectionConfiguration.URL+"fetch_sub_plans";
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(url1, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Response", response.toString());
                pdlg.dismiss();

                try {
                    // JSONObject jsonObject1 = response.getJSONObject(0);
                    if (response.getString("status").equals("success")) {


                        try {
                            JSONArray res1 = response.getJSONArray("data");


                            for (int i = 0; i < res1.length(); i++) {

                                Subscription subscription = new Subscription(res1.getJSONObject(i).getString("id"),
                                        res1.getJSONObject(i).getString("price")
                                        , res1.getJSONObject(i).getString("name"));

                                sub_plan.add(subscription);
                            }


                            List<String> list = new ArrayList<String>();
                            if (list.isEmpty()) {
                                for (int i = 0; i < sub_plan.size(); i++) {
                                    list.add(sub_plan.get(i).getName());
                                }
                            }

                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.item_spinner, list);
                            //dataAdapter.setDropDownViewResource(R.layout.item_spinner);
                            add_sub_scription.setAdapter(dataAdapter);

                            try {
                                add_sub_scription.setSelection(sub_plan.indexOf(0));
                                plan_id1[0] = sub_plan.get(0).getId();

                                add_sub_scription.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        add_sub_scription.setSelection(position);
                                        plan_id1[0] = sub_plan.get(position).getId();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                                if (sub_plan.isEmpty()) {

                                } else {

                                    renew_ok.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog11.dismiss();
                                            PrefManager prefManager = new PrefManager(SubScriptionActivity.this);
                                            String Did = prefManager.getID();
                                            Intent ii = new Intent(SubScriptionActivity.this, MainActivity.class);
                                            ii.putExtra("price", price.getText().toString());
                                            ii.putExtra("driver_id", Did);
                                            ii.putExtra("plan_id", plan_id.getText().toString());
                                            ii.putExtra("plan_nm", plan_nm.getText().toString());

                                            startActivity(ii);
                                            finish();


                                        }
                                    });

                                }


                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }


                    } else {
                        Toast.makeText(SubScriptionActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (JSONException e) {
                    Toast.makeText(SubScriptionActivity.this, "Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        pdlg.dismiss();
                        error.printStackTrace();
                        Log.i("Response", error.toString());
                    }
                });
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonArrayRequest, REQUEST_TAG);


    }

    public int getIndex(String itemName) {
        for (int i = 0; i < sub_plan.size(); i++) {
            Subscription auction = sub_plan.get(i);
            if (itemName.equals(auction.getName())) {
                return i;
            }
        }

        return -1;
    }


    public void fetchBookings() {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait...");
        dialog.show();
        PrefManager prefManager = new PrefManager(SubScriptionActivity.this);
        String Did = prefManager.getID();

        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("driver_id", Did);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        String uu1 =ConnectionConfiguration.URL + "fetch_subscription";
        JsonObjectRequest jsonArrayRequest = new JsonObjectRequest(uu1, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("Response", response.toString());

                dialog.dismiss();

                try {
                    // JSONObject jsonObject1 = response.getJSONObject(0);
                    if (response.getString("status").equals("success")) {


                        try {
                            JSONArray res1 = response.getJSONArray("data");


                            for (int i = 0; i < res1.length(); i++) {

                              /*  Subscription subscription = new Subscription(*/

                                //res1.getJSONObject(i).getString("id");
                                plan_id.setText(res1.getJSONObject(i).getString("plan_id"));
                                from_date.setText(res1.getJSONObject(i).getString("from_date").substring(0, res1.getJSONObject(i).getString("from_date").length() - 8));
                                to_date.setText(res1.getJSONObject(i).getString("to_date").substring(0, res1.getJSONObject(i).getString("to_date").length() - 8));
                                status.setText(res1.getJSONObject(i).getString("status"));
                                plan_nm.setText(res1.getJSONObject(i).getString("name"));
                                price.setText(res1.getJSONObject(i).getString("price"));

                                String first = res1.getJSONObject(i).getString("from_date").substring(0, res1.getJSONObject(i).getString("from_date").length() - 8).replace("-", "/");
                                String second = res1.getJSONObject(i).getString("to_date").substring(0, res1.getJSONObject(i).getString("to_date").length() - 8).replace("-", "/");
                                // getDiff(first, second);

                                String startDate = res1.getJSONObject(i).getString("from_date");//.replace("-", "/");

                                String endDate = res1.getJSONObject(i).getString("to_date");//.replace("-", "/");
                                //  getDiffAll(startDate, endDate);


                                //     SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");


                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date date1 = format.parse(startDate);
                                long timestamp = date1.getTime();

                                Calendar cal = Calendar.getInstance();
                                cal.setTimeInMillis(timestamp);
                                int yr1 = cal.get(Calendar.YEAR);
                                int m1 = cal.get(Calendar.MONTH);
                                int d1 = cal.get(Calendar.DAY_OF_MONTH);

                                int hr = cal.get(Calendar.HOUR_OF_DAY);

                                Date date2 = format.parse(endDate);
                                long timestamp1 = date2.getTime();


                                Calendar cal1 = Calendar.getInstance();
                                cal1.setTimeInMillis(timestamp1);
                                int yr2 = cal1.get(Calendar.YEAR);
                                int m2 = cal1.get(Calendar.MONTH);
                                int d2 = cal1.get(Calendar.DAY_OF_MONTH);

                                int hr1 = cal1.get(Calendar.HOUR_OF_DAY);
                                int yr_diff = yr1 - yr2;
                                int mn_diff = m2 - m1;


                                int dt_diff = d2 - d1;
                                int hr_dff = hr1 - hr;
                                //time_getAll.setText(new DecimalFormat("00").format(mn_diff) + " | " + new DecimalFormat("00").format(dt_diff) + " | " + new DecimalFormat("00").format(hr_dff));

                               String datDiff = (dt_diff+"").replace("-","");
                                String monDiff =(mn_diff+"").replace("-","");

                                month.setText(new DecimalFormat("00").format(Integer.parseInt(monDiff)) + " \nMonths");
                                days.setText(new DecimalFormat("00").format(Integer.parseInt(datDiff)) + "\n Days");
                                hrs.setText(new DecimalFormat("00").format(hr_dff) + " \n Hours");

                               /* if (Integer.parseInt(days.getText().toString()) <= 2) {
                                    renew.setEnabled(true);
                                } else {
                                    renew.setEnabled(false);
                                }*/

                                renew.setEnabled(true);
                                renew.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        Dialog dialog1 = new Dialog(SubScriptionActivity.this);
                                        dialog1.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                                        dialog1.setContentView(R.layout.subscript_layout);
                                        Spinner renew_sub_plan = (Spinner) dialog1.findViewById(R.id.renew_sub_plan);
                                        Button renew_ok = (Button) dialog1.findViewById(R.id.renew_ok);

                                        getSupscription(renew_sub_plan, renew_ok, dialog1);

                                        dialog1.show();




                                      /*  Intent ii= new Intent(SubScriptionActivity.this,RenewActivity.class);
                                        Bundle bb= new Bundle();
                                        bb.putString("plan_id",plan_id.getText().toString());
                                        bb.putString("plan_nm",plan_nm.getText().toString());
                                        ii.putExtras(bb);
                                        startActivity(ii);*/
                                    }
                                });



                            /*    );

                                sub_plan.add(subscription);*/
                            }

                        } catch (Exception ee) {
                            ee.printStackTrace();
                        }


                    } else {
                        Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), "Please Try Again", Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }

            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        dialog.dismiss();
                        error.printStackTrace();
                        Log.i("Response", error.toString());
                    }
                });
        jsonArrayRequest.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonArrayRequest, "REQUEST_TAG");

    }


}
