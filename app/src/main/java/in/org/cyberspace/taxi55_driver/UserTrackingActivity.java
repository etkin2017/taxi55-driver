package in.org.cyberspace.taxi55_driver;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.org.cyberspace.taxi55_driver.application.DAO;
import in.org.cyberspace.taxi55_driver.model.GPSTracker;

public class UserTrackingActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, ResultCallback<LocationSettingsResult>,
        com.google.android.gms.location.LocationListener,
        OnMapReadyCallback {

    String user_id = "";
    GoogleMap mMap;
    Marker m1, m2;
    ImageView back_arrow;
    GPSTracker gps;
    String booking_id = "";
    LatLng pickup_lat_lng, drop_lat_lng;
    String pickUrl = "";
    String dropUrl = "";
    String driverUrl = "";
    Button ride_done;
    String mUrl = "http://maps.googleapis.com/maps/api/geocode/json?address=";
    String from = "", to = "";
    JSONArray jsonArray, jsonArray1;
    JSONObject jsonObject, jsonObject1;
    double myLat = 0.0;
    double myLng = 0.0;
    String myLatLngPnts = "";

    SupportMapFragment mapFragment;
    private GoogleApiClient mGoogleApiClient;
    private final String TAG = "DashBoardFragment";
    private final long INTERVAL = 1000 * 10;//10000
    private final long FASTEST_INTERVAL = 1000 * 5;
    private final long DISPLACEMENT = 1000 * 10;//10000
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;


    boolean check = false;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_tracking);
        back_arrow = (ImageView) findViewById(R.id.back_arrow);
        ride_done = (Button) findViewById(R.id.ride_done);
        user_id = getIntent().getStringExtra("user_id");
        booking_id = getIntent().getStringExtra("booking_id");
        from = getIntent().getStringExtra("from");
        to = getIntent().getStringExtra("to");

        mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        mapFragment.getMapAsync(this);
        back_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        gps = new GPSTracker(UserTrackingActivity.this);
        try {
            pickUrl = mUrl + URLEncoder.encode(from.trim(), "UTF-8") + "&sensor=true";
            Log.e("pickUrl", pickUrl);

            dropUrl = mUrl + URLEncoder.encode(to.trim(), "UTF-8") + "&sensor=true";
            Log.e("dropUrl", dropUrl);

        } catch (Exception ex2) {
            ex2.printStackTrace();
        }

        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        buildLocationSettingsRequest();
        checkLocationSettings();

        ride_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rideDone(booking_id);
            }
        });
        getDesc(pickUrl, dropUrl);

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("UserTracking Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

   /* @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }*/

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        startLocationUpdates();
    }


    @Override
    public void onStart() {
        super.onStart();
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
        Log.d("Onstart", "onStart fired ..............");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull LocationSettingsResult result) {
        final Status status = result.getStatus();
        //                final LocationSettingsStates state = result.getStatus().ge
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // All location settings are satisfied. The client can initialize location
                startLocationUpdates();
                // Toast.makeText(getActivity(), "SUCCESS", Toast.LENGTH_SHORT).show();

                break;
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                // Location settings are not satisfied. But could be fixed by showing the user
                // a dialog.
                //Toast.makeText(getApplicationContext(), "RESOLUTION_REQUIRED", Toast.LENGTH_SHORT).show();

                try {
                    status.startResolutionForResult(UserTrackingActivity.this, 100);

                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location settings are not satisfied. However, we have no way to fix the
                // settings so we won't show the dialog.
                //   Toast.makeText(getApplicationContext(), "SETTINGS_CHANGE_UNAVAILABLE", Toast.LENGTH_SHORT).show();

                break;
        }
    }


    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        Log.d(TAG, "Location update stopped .......................");
    }

    protected void buildLocationSettingsRequest() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }


    protected void startLocationUpdates() {
       /* if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }*/

        if (ActivityCompat.checkSelfPermission((Activity) UserTrackingActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) UserTrackingActivity.this, new String[]{
                    android.Manifest.permission.ACCESS_FINE_LOCATION
            }, 10);
            PendingResult<Status> pendingResult = LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            // pendingResult.

            Log.d(TAG, "Location update started ..............: ");
        }
    }

    protected void checkLocationSettings() {
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        mGoogleApiClient,
                        mLocationSettingsRequest
                );
        result.setResultCallback(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mGoogleApiClient.isConnected()) {
            startLocationUpdates();
            Log.d(TAG, "Location update resumed .....................");
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    class AddressGeocodingTask extends AsyncTask<LatLng, Void, String> {
        Context mContext;
        double latitude, longitude;

        public AddressGeocodingTask(Context context) {
            super();
            mContext = context;
        }

        // Finding address using reverse geocoding
        @Override
        protected String doInBackground(LatLng... params) {
            Geocoder geocoder = new Geocoder(mContext);
            latitude = params[0].latitude;
            longitude = params[0].longitude;

            List<Address> addresses = null;
            String addressText = "";

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (addresses != null && addresses.size() > 0) {
                Address address = addresses.get(0);

                addressText = String.format("%s, %s, %s",
                        address.getMaxAddressLineIndex() > 0 ? address.getAddressLine(0) : "",
                        address.getLocality(),
                        address.getCountryName());


              /*  Address address = addresses.get(0);
                ArrayList<String> addressFragments = new ArrayList<String>();

                for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                    addressFragments.add(address.getAddressLine(i));

                }

                String add1 = TextUtils.join(System.getProperty("line.separator"), addressFragments);
                addressText = add1.replace("\n", ",");*/
            }

            return addressText;
        }

        @Override
        protected void onPostExecute(String addressText) {

            //  mMap.clear();

            try {
                m2.remove();
                addressText = addressText.replace("null,", "");
                //   driverUrl = mUrl + addressText + "&sensor=true";
                Log.e("Address",addressText.toString()+" abc");
                if (!addressText.equals("")) {
                    try {
                        driverUrl = mUrl + URLEncoder.encode(addressText, "UTF-8") + "&sensor=true";
                        Log.e("Driver_Url", driverUrl);
                    } catch (Exception ex2) {
                        ex2.printStackTrace();
                    }
                    getDesc(driverUrl, pickUrl);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public void getDesc(final String pickUrl1, final String dropUrl1) {
        new AsyncTask() {
            JSONObject resPick = null;
            JSONObject resDrop = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();


            }


            @Override
            protected Object doInBackground(Object[] params) {
                resPick = DAO.getResult(pickUrl1);
                resDrop = DAO.getResult(dropUrl1);
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);


                try {

                    jsonArray = resPick.getJSONArray("results");
                    jsonObject = jsonArray.getJSONObject(0);

                    JSONObject jsonObjectLatLng = jsonObject.getJSONObject("geometry");
                    jsonObjectLatLng = jsonObjectLatLng.getJSONObject("location");

                    String referenceOne = jsonObject.getString("formatted_address");
                    jsonArray = jsonObject.getJSONArray("address_components");


                    String latitude = jsonObjectLatLng.getString("lat");
                    String longitude = jsonObjectLatLng.getString("lng");

                    pickup_lat_lng = new LatLng((Double.parseDouble(latitude)), (Double.parseDouble(longitude)))
                    ;

                    try {
                        Log.e("jLat", latitude + "");
                        Log.e("jLng", longitude + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {

                    jsonArray1 = resDrop.getJSONArray("results");
                    jsonObject1 = jsonArray1.getJSONObject(0);

                    JSONObject jsonObjectLatLng1 = jsonObject1.getJSONObject("geometry");
                    jsonObjectLatLng1 = jsonObjectLatLng1.getJSONObject("location");

                    String referenceOne = jsonObject1.getString("formatted_address");
                    jsonArray1 = jsonObject1.getJSONArray("address_components");


                    String latitude1 = jsonObjectLatLng1.getString("lat");
                    String longitude1 = jsonObjectLatLng1.getString("lng");

                    drop_lat_lng = new LatLng((Double.parseDouble(latitude1)), Double.parseDouble(longitude1));
                    ;

                    try {
                        Log.e("jLat1", latitude1 + "");
                        Log.e("jLng1", longitude1 + "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                String url1 = getDirectionsUrl(pickup_lat_lng, drop_lat_lng);

                DownloadTask task = new DownloadTask();
                task.execute(url1);


            }
        }.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 101) {
            switch (resultCode) {
                case Activity.RESULT_OK:

                    mapFragment.getMapAsync(this);
                    break;
                case Activity.RESULT_CANCELED:
                    // TODO
                    //   Toast.makeText(getApplicationContext(),"Cancelled",Toast.LENGTH_LONG).show();
                    checkLocationSettings();
                    break;
            }
        }
    }


    public void rideDone(final String booking_id) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setCancelable(false);
        dialog.setMessage("Please Wait...");
        dialog.show();
        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("current_ride_id", booking_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "set_ride_as_complete", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                dialog.dismiss();
                Log.e("PassengerResponse", response.toString());
                try {
                    Toast.makeText(getApplicationContext(), "Ride Complete Successfully", Toast.LENGTH_LONG).show();

                } catch (Exception e) {

                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                dialog.dismiss();
                error.printStackTrace();
            }
        }

        );

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");

    }

    public void go_to_map(final String user_id) {


        final JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", user_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(ConnectionConfiguration.URL + "fetch_user_lat_long", jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.e("PassengerResponse", response.toString());
                try {
                    JSONArray jsonArray = response.getJSONArray("data");
                    JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                    String latitude = jsonObject1.getString("latitude");
                    String longitude = jsonObject1.getString("longitude");
                    // Toast.makeText(UserTrackingActivity.this, "lat :" + latitude + " lng : " + longitude, Toast.LENGTH_SHORT).show();


                    //   JSONObject jsonObject1 = response.getJSONArray("data").getJSONObject(0);
                    m1 = mMap.addMarker(new MarkerOptions()
                            .title("User")
                            .position(new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude)))
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.start)));
                    //
                    m1.showInfoWindow();

                    LatLng la = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(la));
                    mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));
                    Thread thread = new Thread() {
                        @Override
                        public void run() {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                //sweetAlertDialog.dismiss();
                                e.printStackTrace();
                            }


                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    m1.remove();
                                    go_to_map(user_id);
                                    // Do some stuff
                                   /* sweetAlertDialog.dismiss();
                                    mydecoderview.startCamera();
                                    mydecoderview.setQRDecodingEnabled(true);*/
                                }
                            });
                        }
                    };
                    thread.start(); //start the thread


                } catch (JSONException e) {
                    m1.showInfoWindow();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.printStackTrace();
            }
        }

        );

        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(5000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppSingleton.getInstance(getApplicationContext()).addToRequestQueue(jsonObjectRequest, "BookingRequest");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        if (gps.canGetLocation()) {
            myLat = gps.getLatitude();
            myLng = gps.getLongitude();
            m2 = mMap.addMarker(new MarkerOptions().position(new LatLng(myLat, myLng))
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.carm)
                    )
            );

            m2.showInfoWindow();
            mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()), 10, new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(myLat, myLng)));

                }

                @Override
                public void onCancel() {

                }
            });

            myLatLngPnts = myLat + "," + myLng;
           startThread();
        } else {
            checkLocationSettings();
        }


        // go_to_map(user_id);
    }

    public void startThread() {
        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    //sweetAlertDialog.dismiss();
                    e.printStackTrace();
                }


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //   mapFragment.getMapAsync(UserTrackingActivity.this);

                        check = true;
                        AddressGeocodingTask task = new AddressGeocodingTask(UserTrackingActivity.this);
                        task.execute(new LatLng(myLat, myLng));
                          startThread();
                    }
                });
            }
        };
        thread.start(); //start the thread

    }

    private int calculateZoomLevel() {

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;
        double equatorLength = 40075004; // in meters
        double widthInPixels = width;
        double metersPerPixel = equatorLength / 256;
        int zoomLevel = 1;
        while ((metersPerPixel * widthInPixels) > 20000) {
            metersPerPixel /= 2;
            ++zoomLevel;
        }
        Log.i("ADNAN", "zoom level = " + zoomLevel);
        return zoomLevel;
    }


    private String getDirectionsUrl(LatLng origin, LatLng dest) {


        // Origin of route
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;

        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;

        // Sensor enabled
        String sensor = "sensor=false";

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        return url;
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Excep downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    private class DownloadTask extends AsyncTask<String, Void, String> {

        // Downloading data in non-ui thread

        String urlP = "";

        @Override
        protected String doInBackground(String... url) {

            // For storing data from web service
            String data = "";

            urlP = url[0];

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        // Executes in UI thread, after the execution of
        // doInBackground()
        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);


            if (check == true) {
                ParserTask1 parserTask1 = new ParserTask1();
                parserTask1.execute(result);


            } else {
                ParserTask parserTask = new ParserTask();
                parserTask.execute(result);
            }
        }
    }

    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {


            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            if (result.size() < 1) {
                Toast.makeText(getApplicationContext(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {    // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duration = (String) point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.RED);
            }

            //   tvDistanceDuration.setText("Distance:"+distance + ", Duration:"+duration);
            Log.e("print_distance", "Distance:" + distance + ", Duration:" + duration);

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);

            //   if(pickup_lat_lng)
            MarkerOptions options = new MarkerOptions();
            options.position(pickup_lat_lng);
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.start));
            mMap.addMarker(options);
              mMap.moveCamera(CameraUpdateFactory.newLatLng(pickup_lat_lng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()));

            options = new MarkerOptions();
            options.position(drop_lat_lng);
            options.title(to);
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.grren_pin3));
            mMap.addMarker(options);

            //  mMap.animateCamera(CameraUpdateFactory.zoomTo(13f));

        }


    }


    private class ParserTask1 extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                // Starts parsing data
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        // Executes in UI thread, after the parsing process
        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {


            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();
            String distance = "";
            String duration = "";

            if (result.size() < 1) {
                Toast.makeText(getApplicationContext(), "No Points", Toast.LENGTH_SHORT).show();
                return;
            }

            // Traversing through all the routes
            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList<LatLng>();
                lineOptions = new PolylineOptions();

                // Fetching i-th route
                List<HashMap<String, String>> path = result.get(i);

                // Fetching all the points in i-th route
                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                    if (j == 0) {    // Get distance from the list
                        distance = (String) point.get("distance");
                        continue;
                    } else if (j == 1) { // Get duration from the list
                        duration = (String) point.get("duration");
                        continue;
                    }

                    double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);

                    points.add(position);
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points);
                lineOptions.width(5);
                lineOptions.color(Color.MAGENTA);
            }

            //   tvDistanceDuration.setText("Distance:"+distance + ", Duration:"+duration);
            Log.e("print_distance", "Distance:" + distance + ", Duration:" + duration);

            // Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);

            //   if(pickup_lat_lng)
            m2.remove();
            MarkerOptions options = new MarkerOptions();
            options.position(pickup_lat_lng);
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.carm));
            mMap.addMarker(options);
            /// mMap.moveCamera(CameraUpdateFactory.newLatLng(pickup_lat_lng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(calculateZoomLevel()), 10, new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {
                  //  mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(myLat, myLng)));

               //     startThread();
                }

                @Override
                public void onCancel() {

                }
            });


            //  mMap.animateCamera(CameraUpdateFactory.zoomTo(13f));

        }


    }


}
