package in.org.cyberspace.taxi55_driver.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import in.org.cyberspace.taxi55_driver.R;

/**
 * Created by Ashvini on 3/17/2017.
 */

public class ActiveBookingAdapter extends BaseAdapter {
    
  
    ArrayList<String> ID,Dates,Times,Amt,KM,FROM,TO;
    Activity mContex;
    public ActiveBookingAdapter(Activity context, ArrayList<String> id, ArrayList<String>dates, ArrayList<String> times, ArrayList<String>amt, ArrayList<String>km, ArrayList<String> from, ArrayList<String> to)
    {
        this.mContex=context;
        this.ID=id;
        this.Dates=dates;
        this.Times=times;
        this.Amt=amt;
        this.KM=km;
        this.FROM=from;
        this.TO=to;

        Log.e("Adapter AMT ",Amt.size()+"");
    }
    
    @Override
    public int getCount() {
        return ID.size();
    }

    @Override
    public Object getItem(int position) {
        return ID.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater layoutInflater = mContex.getLayoutInflater();
        View row=layoutInflater.inflate(R.layout.active_booking_details_row,parent,false);
        TextView tvID,tvDate,tvTime,tvAmt,bookStatus;//tvKM,tvFrom,tvTo;

        tvID=(TextView)row.findViewById(R.id.bookID);
        tvDate=(TextView)row.findViewById(R.id.bookDate);
        tvTime=(TextView)row.findViewById(R.id.bookTime);
        tvAmt=(TextView)row.findViewById(R.id.bookAmt);
        bookStatus =(TextView) row.findViewById(R.id.bookStatus);
       /* tvKM=(TextView)row.findViewById(R.id.tvDist);
        tvFrom=(TextView)row.findViewById(R.id.tvFrom);
        tvTo=(TextView)row.findViewById(R.id.tvTo);*/

        tvID.setText(ID.get(position));
        tvID.setTextColor(Color.RED);
        tvDate.setText("Date : "+Dates.get(position));
        tvTime.setText("Time : "+Times.get(position));
        tvAmt.setText("Amount : "+Amt.get(position));
        bookStatus.setText("Status : "+KM.get(position));
     /*   tvFrom.setText("From : "+FROM.get(position));
        tvTo.setText("To : "+TO.get(position));*/


        return row;
    }
}
