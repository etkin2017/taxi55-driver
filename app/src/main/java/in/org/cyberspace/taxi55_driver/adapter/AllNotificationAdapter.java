package in.org.cyberspace.taxi55_driver.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import in.org.cyberspace.taxi55_driver.R;
import in.org.cyberspace.taxi55_driver.ShowRequestFromUserActivity;
import in.org.cyberspace.taxi55_driver.model.RequestMessage;

/**
 * Created by Ashvini on 3/18/2017.
 */

public class AllNotificationAdapter extends RecyclerView.Adapter<AllNotificationAdapter.ViewHolder> {

    List<RequestMessage> requestMessageList;
    Context mCont;

    public AllNotificationAdapter(List<RequestMessage> requestMessageList, Context mCont) {
        this.requestMessageList = requestMessageList;
        this.mCont = mCont;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mCont).inflate(R.layout.msg_layout_row, parent, false);
        ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {

        holder.note_tm.setText(requestMessageList.get(position).getTim1());
        holder.note_message.setText(requestMessageList.get(position).getMessagel().replace("\n", " "));
        holder.note_from.setText(requestMessageList.get(position).getBooking_id());


        holder.notification_container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mCont, ShowRequestFromUserActivity.class);
                intent.putExtra("lat", requestMessageList.get(position).getLat());
                intent.putExtra("lng", requestMessageList.get(position).getLng());
                intent.putExtra("booking_id", requestMessageList.get(position).getBooking_id());
                intent.putExtra("name1", requestMessageList.get(position).getUnser_nm());
                intent.putExtra("drop_location", requestMessageList.get(position).getDrop_location());
                intent.putExtra("date", requestMessageList.get(position).getDate());
                intent.putExtra("message", requestMessageList.get(position).getMessagel());
                intent.putExtra("drop_lat", requestMessageList.get(position).getDrop_lat());
                intent.putExtra("drop_lng", requestMessageList.get(position).getDrop_lng());
                intent.putExtra("dist", requestMessageList.get(position).getDist());
                intent.putExtra("amount", requestMessageList.get(position).getAmt());
                intent.putExtra("time", requestMessageList.get(position).getTime());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mCont.startActivity(intent);

            }
        });


    }

    @Override
    public int getItemCount() {
        return requestMessageList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView note_from, note_message, note_tm;
        ImageView note_image;
        LinearLayout notification_container;

        public ViewHolder(View itemView) {
            super(itemView);
            note_from = (TextView) itemView.findViewById(R.id.note_from);
            note_message = (TextView) itemView.findViewById(R.id.not_message);
            note_tm = (TextView) itemView.findViewById(R.id.note_timing);

            note_image = (ImageView) itemView.findViewById(R.id.note_image_view);
            notification_container = (LinearLayout) itemView.findViewById(R.id.notification_container);

        }
    }
}
