package in.org.cyberspace.taxi55_driver.application;

/**
 * Created by Ashvini on 3/2/2017.
 */

public class Config {

    public static boolean appendNotificationMessages = true;
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String INSERT_MSG ="http://shrisadgurucoaching.com/fcm/fcmp.php";

}
