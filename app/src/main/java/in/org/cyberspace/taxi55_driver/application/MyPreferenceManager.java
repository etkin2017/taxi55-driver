package in.org.cyberspace.taxi55_driver.application;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Ashvini on 3/2/2017.
 */

public class MyPreferenceManager {


    private String TAG = MyPreferenceManager.class.getSimpleName();

    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    SharedPreferences.Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "androidhive_gcm";

    // All Shared Preferences Keys
    private static final String KEY_USER_ID = "user_id";
    private static final String KEY_USER_NAME = "user_name";
    private static final String KEY_USER_EMAIL = "user_email";
    private static final String KEY_NOTIFICATIONS = "notifications";
    private static final String KEY_NOTIFICATION_BY_ID = "notifications_by_id";
    private static final String KEY_NOTIFICATION_BY_ID_GROUP = "notifications_by_group";

    private static final String KEY_NOTIFICATION_BY_ADMIN = "admin_notification";


    // Constructor
    public MyPreferenceManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }


    public void addNotification(String notification) {

        // get old notifications
        String oldNotifications = getNotifications();

        if (oldNotifications != null) {
            oldNotifications += "|" + notification;
        } else {
            oldNotifications = notification;
        }

        editor.putString(KEY_NOTIFICATIONS, oldNotifications);
        editor.commit();
    }


    public void removeNotimessage() {
        editor.remove(KEY_NOTIFICATIONS);
        editor.apply();
        editor.commit();
    }

    public void addNotificationById(String id) {

        // get old notifications
        String oldNotifications = getNotificationsById(id);

        if (oldNotifications != null) {
            oldNotifications += "|" + id;
        } else {
            oldNotifications = id;
        }

        editor.putString(KEY_NOTIFICATION_BY_ID + id, oldNotifications);
        editor.commit();
    }

    public void addNotificationfromAdmin(String g_id) {

        // get old notifications
        String oldNotifications = getNotificationsByGroup(g_id);

        if (oldNotifications != null) {
            oldNotifications += "|" + g_id;
        } else {
            oldNotifications = g_id;
        }

        editor.putString(KEY_NOTIFICATION_BY_ADMIN + g_id, oldNotifications);
        editor.commit();
    }


    public void removeNotificaion(String key) {
        editor.remove(KEY_NOTIFICATION_BY_ID + key);
        editor.apply();
        editor.commit();
    }


    public void removeUserNGroupNotificaion(String key) {
        editor.remove(KEY_NOTIFICATION_BY_ADMIN + key);
        editor.apply();
        editor.commit();
    }


    public String getNotifications() {
        return pref.getString(KEY_NOTIFICATIONS, null);
    }

    public String getNotificationsById(String id) {
        return pref.getString(KEY_NOTIFICATION_BY_ID + id, null);
    }

    public String getNotificationsByGroup(String id) {
        return pref.getString(KEY_NOTIFICATION_BY_ADMIN + id, null);
    }

    public void clear() {
        editor.clear();
        editor.commit();
    }
}
