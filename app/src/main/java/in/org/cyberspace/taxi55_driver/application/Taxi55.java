package in.org.cyberspace.taxi55_driver.application;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Ashvini on 3/2/2017.
 */

public class Taxi55 extends Application {

    private static Taxi55 mInstance;
    private static Context myContext;

    private MyPreferenceManager pref;
    public static final String TAG = Taxi55.class.getSimpleName();


    public MyPreferenceManager getPrefManager() {
        if (pref == null) {
            pref = new MyPreferenceManager(this);
        }

        return pref;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        myContext = getApplicationContext();

        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                handleUncaughtException(thread, e);
            }
        });
    }

    public void handleUncaughtException(Thread thread, Throwable e) {
        e.printStackTrace(); // not all Android versions will print the stack trace automatically

        String thread_stack_trace = thread.getStackTrace().toString();

        String thrw = e.getMessage();

        try {
            Log.e("thread_stack_trace", thread_stack_trace);
            Log.e("thrw", thrw + " throwable " + e.getLocalizedMessage());

        } catch (Exception ee) {
            ee.printStackTrace();
        }

        Intent intent = new Intent();
        //     intent.setAction ("com.mydomain.SEND_LOG"); // see step 5.
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // required when starting from Application
        startActivity(intent);

        System.exit(1); // kill off the crashed app
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        //  MultiDex.install(this);
    }

    public static synchronized Taxi55 getInstance() {
        return mInstance;
    }


   /* public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }*/

}
