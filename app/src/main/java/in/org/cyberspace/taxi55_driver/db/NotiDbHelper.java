package in.org.cyberspace.taxi55_driver.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

import in.org.cyberspace.taxi55_driver.DBHelper;
import in.org.cyberspace.taxi55_driver.MapActivity;
import in.org.cyberspace.taxi55_driver.PrefManager;
import in.org.cyberspace.taxi55_driver.model.NewMessage;
import in.org.cyberspace.taxi55_driver.model.RequestMessage;
import in.org.cyberspace.taxi55_driver.model.User;
import okhttp3.Response;

/**
 * Created by Ashvini on 2/26/2017.
 */

public class NotiDbHelper extends SQLiteOpenHelper {

    static String DATABASE_NAME = "noti.db";


    public static final String TABLE_NAME_TWo = "noti";
    public static final String NEXT = "next_id";

    public static final String KEY_LNAME = "lname";
    public static final String KEY_ID = "id";
    public static final String TABLE_NAME = "users";
    public static final String CHAT_ROOM_ID = "chat_room_id";
    public static final String MESSAGE = "message";
    public static final String CREATED_AT = "created_at";

    public static final String messageTable = "message";
    public static final String mid = "id";
    public static final String sid = "sid";
    public static final String rid = "rid";
    public static final String msg = "message";
    public static final String stime = "stime";
    public static final String rtime = "rtime";
    public static final String mtypr = "type";
    public static final String uid = "uid";
    public static final String chk_image = "chk_image";
    public static final String rs = "rs";
    public static final String r_name = "r_name";
    Context mctx;

    // for storing messages of NotiDbHelper
    public static final String passenger_table = "passenger_noti";
    public static final String msg1 = "msg";
    public static final String booking_id = "booking_id";
    public static final String pass_name = "passenger_nm";
    public static final String time1 = "time";
    public static final String msgid = "msg_id";
    public static final String msg_type = "msg_type";
    public static final String lat = "lat";
    public static final String lng = "lng";
    public static final String drop_location = "drop_location";
    public static final String date = "date2";
    public static final String drop_lat = "drop_lat";
    public static final String drop_lng = "drop_lng";
    public static final String dist = "dist";
    public static final String amt = "amt";
    public static final String time = "time132";

    public NotiDbHelper(Context context) {
        super(context, DATABASE_NAME, null, 2);
        mctx = context;
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String MESSAGE = "CREATE TABLE " + messageTable + " (" + mid + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + sid + " TEXT, " + rid + " TEXT, " + msg + " TEXT ," + stime + " DATETIME DEFAULT CURRENT_TIMESTAMP ," + rtime + " DATETIME DEFAULT CURRENT_TIMESTAMP ," + uid + " INTEGER , " + rs + " INTEGER  , " + r_name +
                " TEXT )";

        String PASSERNGER_MESSAGE = "CREATE TABLE " + passenger_table + " (" + msgid + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " + msg1 + " TEXT , "
                + booking_id + " TEXT , " + pass_name + " TEXT , " + time1 + " TEXT ," + msg_type + " INTEGER , " + lat + " TEXT , "
                + lng + " TEXT , " + drop_location + " TEXT , " + date + " TEXT , " + drop_lat + " TEXT , " + drop_lng + " TEXT , " + dist + " TEXT , " +
                amt + " TEXT , " + time + " TEXT )";

        db.execSQL(MESSAGE);
        db.execSQL(PASSERNGER_MESSAGE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + MESSAGE);
        db.execSQL("DROP TABLE IF EXISTS " + passenger_table);
        onCreate(db);
    }


    public void passengerMessage(RequestMessage msgs) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(msg1, msgs.getMessagel());
        values.put(booking_id, msgs.getBooking_id());
        values.put(pass_name, msgs.getUnser_nm());
        values.put(time1, msgs.getTim1());
        values.put(msg_type, msgs.getType());
        values.put(lat, msgs.getLat());
        values.put(lng, msgs.getLng());
        values.put(drop_location, msgs.getDrop_location());
        values.put(date, msgs.getDate());
        values.put(drop_lat, msgs.getDrop_lat());
        values.put(drop_lng, msgs.getDrop_lng());
        values.put(dist, msgs.getDist());
        values.put(amt, msgs.getAmt());
        values.put(time, msgs.getTime());
        db.insert(passenger_table, null, values);
        db.close();

    }


    public ArrayList<RequestMessage> getAllPassgenerMessage() {

        ArrayList<RequestMessage> mmm = new ArrayList<RequestMessage>();
        try {
            String query = "select * from " + passenger_table + "  ORDER BY   msg_id  DESC ";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    RequestMessage message1 = new RequestMessage(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getInt(5)
                            , cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10), cursor.getString(11)
                            , cursor.getString(12), cursor.getString(13), cursor.getString(14));
                    mmm.add(message1);
                    //Log.e("msg", message1.toString());

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mmm;
    }

    public void delete_all_values() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from " + passenger_table);
    }


    public void saveSingleMessage(NewMessage msgs) {
        SQLiteDatabase db = this.getWritableDatabase();


        ContentValues values = new ContentValues();

        values.put(sid, msgs.getSid());
        values.put(rid, msgs.getRid());
        values.put(msg, msgs.getMsg());
        values.put(stime, msgs.getStime());
        values.put(rtime, msgs.getRtime());
        values.put(uid, msgs.getUid());
        values.put(rs, msgs.getRs());
        values.put(r_name, msgs.getR_name());
        db.insert(messageTable, null, values);
        db.close();
    }

    public ArrayList<NewMessage> getMessageByRid_Sid(String msid, String mrid) {

        ArrayList<NewMessage> mmm = new ArrayList<NewMessage>();
        try {
            String query = "select * from " + messageTable + " where " + sid + " = '" + msid + "' AND " + rid + " = '" + mrid + "'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor cursor = db.rawQuery(query, null);
            if (cursor.moveToFirst()) {
                do {
                    NewMessage message1 = new NewMessage(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getInt(6), cursor.getInt(7), cursor.getString(8));
                    mmm.add(message1);
                    //Log.e("msg", message1.toString());

                } while (cursor.moveToNext());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mmm;
    }

    public ArrayList<User> getUserByChat() {
        String query = "select DISTINCT(rid) from " + messageTable + "  ORDER BY   id  DESC ";
        ArrayList<User> u1 = new ArrayList<User>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {


                DBHelper dbHelper = new DBHelper(mctx);
                HashMap<String, String> mobile_data = dbHelper.get_userData();
                String m1 =cursor.getString(0);
                if (!mobile_data.get("mobile").equals(m1)) {
                    User user = getUserById(cursor.getString(0));

                    if (user != null) {
                        u1.add(user);
                        //Log.e("usersnoti", user.toString());
                    }
                }


            } while (cursor.moveToNext());
        }
        return u1;

        //return null;
    }


    public User getUserById(String rid) {
        User user = null;
        String query = "select r_name from " + messageTable + " WHERE  rid = '" + rid + "' AND rs = " + 1;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            //String name, String image, int type, int id, String mobile, String status,int aid
            // id + " INTEGER, " + name + " TEXT," + image + " TEXT," + mobile + " TEXT," + type + " INTEGER ,
            // " + ustatus + " TEXT ,
            // " + aid + " INTEGER)";


            user = new User(rid, cursor.getString(0));

        }
        return user;
    }


}
