package in.org.cyberspace.taxi55_driver.fragments;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalFuturePaymentActivity;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;

import java.math.BigDecimal;

import in.org.cyberspace.taxi55_driver.R;

import static in.org.cyberspace.taxi55_driver.model.PayPalConfig.CONFIG_ENVIRONMENT;
import static in.org.cyberspace.taxi55_driver.model.PayPalConfig.PAYPAL_CLIENT_ID;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentFragment extends Fragment {

    View v;
    private static final int REQUEST_CODE_PAYMENT = 1;
    private static final int REQUEST_CODE_FUTURE_PAYMENT = 2;
    //- See more at: http://www.theappguruz.com/blog/integrate-paypal-in-android#sthash.5wO4fj5r.dpuf

    public PaymentFragment() {
        // Required empty public constructor
    }


    private static PayPalConfiguration config = new PayPalConfiguration()
            .environment(CONFIG_ENVIRONMENT)
            .clientId(PAYPAL_CLIENT_ID)
// the following are only used in PayPalFuturePaymentActivity.
            .merchantName("Hipster Store")
            .merchantPrivacyPolicyUri(
                    Uri.parse("https://www.example.com/privacy"))
            .merchantUserAgreementUri(
                    Uri.parse("https://www.example.com/legal"));
    PayPalPayment thingToBuy;
    //  - See more at: http://www.theappguruz.com/blog/integrate-paypal-in-android#sthash.5wO4fj5r.dpuf

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_payment, container, false);

        Intent intent = new Intent(getActivity(), PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        getActivity().startService(intent);

        return v;
    }


    public void onFuturePaymentPressed(View pressed) {
        Intent intent = new Intent(getActivity(),
                PayPalFuturePaymentActivity.class);
        startActivityForResult(intent, REQUEST_CODE_FUTURE_PAYMENT);
    }
    // - See more at: http://www.theappguruz.com/blog/integrate-paypal-in-android#sthash.5wO4fj5r.dpuf



}
//- See more at: http://www.theappguruz.com/blog/integrate-paypal-in-android#sthash.5wO4fj5r.dpuf
//}
