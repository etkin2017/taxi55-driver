package in.org.cyberspace.taxi55_driver.model;

/**
 * Created by Ashvini on 3/30/2017.
 */

public class CarModel {

    String car_id,car_type,car_img_nm;

    public CarModel(String car_id, String car_type, String car_img_nm) {
        this.car_id = car_id;
        this.car_type = car_type;
        this.car_img_nm = car_img_nm;
    }

    public String getCar_id() {
        return car_id;
    }

    public void setCar_id(String car_id) {
        this.car_id = car_id;
    }

    public String getCar_type() {
        return car_type;
    }

    public void setCar_type(String car_type) {
        this.car_type = car_type;
    }

    public String getCar_img_nm() {
        return car_img_nm;
    }

    public void setCar_img_nm(String car_img_nm) {
        this.car_img_nm = car_img_nm;
    }
}
