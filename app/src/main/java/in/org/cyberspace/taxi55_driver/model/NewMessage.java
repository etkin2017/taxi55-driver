package in.org.cyberspace.taxi55_driver.model;

/**
 * Created by Ashvini on 2/26/2017.
 */

public class NewMessage {

    private int id;
    private String sid;
    private String rid;
    private String msg;
    private String stime;
    private String rtime;
    private int mtypr;
    private int uid;
    private String r_name;
    int rs;


    public String getR_name() {
        return r_name;
    }

    public void setR_name(String r_name) {
        this.r_name = r_name;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    @Override
    public String toString() {
        return "NewMessage{" +
                "id=" + id +
                ", sid=" + sid +
                ", rid=" + rid +
                ", msg='" + msg + '\'' +
                ", stime='" + stime + '\'' +
                ", rtime='" + rtime + '\'' +
                ", mtypr=" + mtypr +
                ", uid=" + uid +
                ",rs=" + rs +
                ", r_name = " + r_name +
                '}';
    }

    public int getRs() {
        return rs;
    }

    public void setRs(int rs) {
        this.rs = rs;
    }

    public NewMessage(int id, String sid, String rid, String msg, String stime, String rtime, int uid, int rs, String r_name) {
        this.id = id;
        this.sid = sid;
        this.rid = rid;
        this.msg = msg;
        this.stime = stime;
        this.rtime = rtime;

        this.uid = uid;

        this.rs = rs;
        this.r_name = r_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getStime() {
        return stime;
    }

    public void setStime(String stime) {
        this.stime = stime;
    }

    public String getRtime() {
        return rtime;
    }

    public void setRtime(String rtime) {
        this.rtime = rtime;
    }

    public int getMtypr() {
        return mtypr;
    }

    public void setMtypr(int mtypr) {
        this.mtypr = mtypr;
    }

}
