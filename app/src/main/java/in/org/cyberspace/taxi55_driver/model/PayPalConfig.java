package in.org.cyberspace.taxi55_driver.model;

import com.paypal.android.sdk.payments.PayPalConfiguration;

/**
 * Created by Ashvini on 2/26/2017.
 */

public class PayPalConfig {
    public static final String PAYPAL_CLIENT_ID = "YOUR PAYPAL CLIENT ID";

    public static final String CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;// - See more at: http://www.theappguruz.com/blog/integrate-paypal-in-android#sthash.5wO4fj5r.dpuf
}
