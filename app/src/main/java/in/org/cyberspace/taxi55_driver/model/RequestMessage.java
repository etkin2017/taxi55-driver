package in.org.cyberspace.taxi55_driver.model;

/**
 * Created by Ashvini on 3/18/2017.
 */

public class RequestMessage {

    String messagel, booking_id, unser_nm, tim1;

    int mess_id, type;


    String lat, lng, drop_location, date, drop_lat, drop_lng, dist, amt, time;


    public RequestMessage(int mess_id, String messagel, String booking_id, String unser_nm, String tim1, int type, String lat, String lng, String drop_location, String date, String drop_lat, String drop_lng, String dist, String amt, String time) {
        this.messagel = messagel;
        this.booking_id = booking_id;
        this.unser_nm = unser_nm;
        this.tim1 = tim1;
        this.mess_id = mess_id;
        this.type = type;
        this.lat = lat;
        this.lng = lng;
        this.drop_location = drop_location;
        this.date = date;
        this.drop_lat = drop_lat;
        this.drop_lng = drop_lng;
        this.dist = dist;
        this.amt = amt;
        this.time = time;
    }


    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getDrop_location() {
        return drop_location;
    }

    public void setDrop_location(String drop_location) {
        this.drop_location = drop_location;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDrop_lat() {
        return drop_lat;
    }

    public void setDrop_lat(String drop_lat) {
        this.drop_lat = drop_lat;
    }

    public String getDrop_lng() {
        return drop_lng;
    }

    public void setDrop_lng(String drop_lng) {
        this.drop_lng = drop_lng;
    }

    public String getDist() {
        return dist;
    }

    public void setDist(String dist) {
        this.dist = dist;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    public int getMess_id() {
        return mess_id;
    }

    public void setMess_id(int mess_id) {
        this.mess_id = mess_id;
    }

    public String getMessagel() {
        return messagel;
    }

    public void setMessagel(String messagel) {
        this.messagel = messagel;
    }

    public String getBooking_id() {
        return booking_id;
    }

    public void setBooking_id(String booking_id) {
        this.booking_id = booking_id;
    }

    public String getUnser_nm() {
        return unser_nm;
    }

    public void setUnser_nm(String unser_nm) {
        this.unser_nm = unser_nm;
    }

    public String getTim1() {
        return tim1;
    }

    public void setTim1(String tim1) {
        this.tim1 = tim1;
    }
}
