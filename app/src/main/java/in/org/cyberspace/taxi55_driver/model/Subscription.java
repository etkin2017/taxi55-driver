package in.org.cyberspace.taxi55_driver.model;

/**
 * Created by Ashvini on 2/26/2017.
 */

public class Subscription {
    String id;
    String price;
    String name;

    String plan_id, from_date, to_date, status;

    public Subscription(String id, String plan_id, String from_date, String to_date, String status, String name, String price) {
        this.id = id;
        this.plan_id = plan_id;
        this.from_date = from_date;
        this.to_date = to_date;
        this.status = status;
        this.name = name;
        this.price = price;
    }

    public String getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Subscription() {

    }

    public Subscription(String id, String price, String name) {
        this.id = id;
        this.price = price;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
