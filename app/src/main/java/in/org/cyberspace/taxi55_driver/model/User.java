package in.org.cyberspace.taxi55_driver.model;

/**
 * Created by Ashvini on 3/8/2017.
 */

public class User {

    String contact,name;

    public User(String contact, String name) {
        this.contact = contact;
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
