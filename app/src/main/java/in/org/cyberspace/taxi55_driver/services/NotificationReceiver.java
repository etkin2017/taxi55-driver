package in.org.cyberspace.taxi55_driver.services;

/**
 * Created by Ashvini on 2/26/2017.
 */

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import in.org.cyberspace.taxi55_driver.ChatActivity;
import in.org.cyberspace.taxi55_driver.DBHelper;
import in.org.cyberspace.taxi55_driver.Home;
import in.org.cyberspace.taxi55_driver.NewChatActivity;
import in.org.cyberspace.taxi55_driver.PrefManager;
import in.org.cyberspace.taxi55_driver.R;
import in.org.cyberspace.taxi55_driver.ShowRequestFromUserActivity;
import in.org.cyberspace.taxi55_driver.Splash;
import in.org.cyberspace.taxi55_driver.application.MyPreferenceManager;
import in.org.cyberspace.taxi55_driver.db.NotiDbHelper;
import in.org.cyberspace.taxi55_driver.model.NewMessage;
import in.org.cyberspace.taxi55_driver.model.NotificationUtils;
import in.org.cyberspace.taxi55_driver.model.RequestMessage;


public class NotificationReceiver extends FirebaseMessagingService {
    String TAG = "NotificationReceiver";
    public static final String name = "Notification";
    public static final String name2 = "Note2";
    public static final String name3 = "Notification3";
    private NotificationUtils notificationUtils;
    Intent intent;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.e("message_received", remoteMessage.getData() + "");


        Log.e(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Message data payload: " + remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }


        String chatRoomId = remoteMessage.getData().get("chatroomid");

        if (chatRoomId.equals("from_user")) {

            String lat = remoteMessage.getData().get("lat");
            String lng = remoteMessage.getData().get("lng");
            String booking_id = remoteMessage.getData().get("booking_id");
            String mmmmm = remoteMessage.getData().get("message");
            String name1 = remoteMessage.getData().get("name");
            String drop_location = remoteMessage.getData().get("drop_location");
            String date = remoteMessage.getData().get("date");
            String drop_lat = remoteMessage.getData().get("drop_lat");
            String drop_lng = remoteMessage.getData().get("drop_lng");
            String amt = remoteMessage.getData().get("amount");
            String time = remoteMessage.getData().get("time");
            String dist = remoteMessage.getData().get("dist");

            try {
                MyPreferenceManager preferenceManager = new MyPreferenceManager(getApplicationContext());
                preferenceManager.addNotificationfromAdmin("from_user");

            } catch (Exception ee) {
                ee.printStackTrace();
            }
            try {

                Calendar cal = Calendar.getInstance();
                int hr = cal.get(Calendar.HOUR_OF_DAY);
                int min = cal.get(Calendar.MINUTE);

                String tm = hr + ":" + min;
                //  DecimalFormat format = new DecimalFormat("##.##");

                //  String t1 = format.format(tm);

                // t1= t1.replace(".",":");
                RequestMessage requestMessage = new RequestMessage(0, mmmmm, booking_id, name1, tm, 0, lat, lng, drop_location, date, drop_lat, drop_lng, dist, amt, time);
                NotiDbHelper helper = new NotiDbHelper(this);
                helper.passengerMessage(requestMessage);

            } catch (Exception wq) {
                wq.printStackTrace();
            }

            try {
                sendBroadcast(new Intent()
                        .setAction(name3)
                        .addCategory(Intent.CATEGORY_DEFAULT)
                        .putExtra("data1", "yes"));
            } catch (Exception es) {
                es.printStackTrace();
            }


            Intent intent = new Intent(getApplicationContext(), ShowRequestFromUserActivity.class);
            intent.putExtra("lat", lat);
            intent.putExtra("lng", lng);
            intent.putExtra("booking_id", booking_id);
            intent.putExtra("name1", name1);
            intent.putExtra("drop_location", drop_location);
            intent.putExtra("date", date);
            intent.putExtra("message", mmmmm);
            intent.putExtra("drop_lat", drop_lat);
            intent.putExtra("drop_lng", drop_lng);
            intent.putExtra("dist", dist);
            intent.putExtra("amount", amt);
            intent.putExtra("time", time);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            sendNotification("Message From Passenger", mmmmm, intent);


        } else {

            String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
            String rid = remoteMessage.getData().get("rid");
            String sid = remoteMessage.getData().get("sid");
            String rtime = remoteMessage.getData().get("rtime");

            String mmmmm = remoteMessage.getData().get("message");
            String name1 = remoteMessage.getData().get("name");
            NotiDbHelper mHelper = new NotiDbHelper(getApplicationContext());
            NewMessage m = new NewMessage(0, rid, sid, mmmmm, timeStamp + "", timeStamp + "", 233, 1, name1);
            mHelper.saveSingleMessage(m);

            //    sendNotification("Taxi55", mmmmm + "");
            SharedPreferences sh1 = getApplicationContext().getSharedPreferences("chat_rm", Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sh1.edit();
      /*  editor.putString("rid",chatRoomId+"");
        editor.putString("sid",id+"");
        editor.putString("type",type+"");
        */
            String _rid = sh1.getString("rid", "");
            //Log.e("_rid", _rid);
            String _sid = sh1.getString("sid", "");

            Intent intentres = new Intent(getApplicationContext(), ChatActivity.class);
      /*  intentres.putExtra("rid", rid);
        intentres.putExtra("sid", sid);*/
            showNotificationMessage(getApplicationContext(), "Message from Passenger : ", name1 + " : " + mmmmm, sid + "", rtime, intentres);

            sendBroadcast(new Intent()
                    .setAction(name)
                    .addCategory(Intent.CATEGORY_DEFAULT)
                    .putExtra("data", "yes"));

        }


       /* sendBroadcast(new Intent()
                .setAction(name2)
                .addCategory(Intent.CATEGORY_DEFAULT)
                .putExtra("data", "yes"));*/

      /*  if ((_rid.equals(sid)) && (_sid.equals(rid)) && (NewChatActivity.activate1 == true)) {
            sendBroadcast(new Intent()
                    .setAction(name)
                    .addCategory(Intent.CATEGORY_DEFAULT)
                    .putExtra("data", "yes"));
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();


        } else if ((_rid.equals(rid)) && (NewChatActivity.activate1 == true)) {
            sendBroadcast(new Intent()
                    .setAction(name)
                    .addCategory(Intent.CATEGORY_DEFAULT)
                    .putExtra("data", "yes"));
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.playNotificationSound();


        }*/


    }

    private void sendNotification(String messageBody, String message, Intent intent11) {


        int notify_no = 1;

        PrefManager prefManager = new PrefManager(NotificationReceiver.this);

        boolean chk_login = prefManager.isUserLoggedIn();

        if (chk_login == true) {
            intent = intent11;
        } else {
            intent = new Intent(this, Splash.class);
        }


        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, notify_no /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        if (notify_no < 9) {
            notify_no = notify_no + 1;
        } else {
            notify_no = 0;
        }

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.icon)
                .setContentTitle(messageBody)
                .setContentText(message)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(notify_no + 2 /* ID of notification */, notificationBuilder.build());
    }

    private void showNotificationMessage(Context context, String title, String message, String id, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, id, timeStamp, intent);
    }

}
