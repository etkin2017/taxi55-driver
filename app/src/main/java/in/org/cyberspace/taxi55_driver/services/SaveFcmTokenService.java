package in.org.cyberspace.taxi55_driver.services;

import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Ashvini on 2/25/2017.
 */

public class SaveFcmTokenService extends FirebaseInstanceIdService {

    String TAG = "SaveFcmTokenService";

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Refreshed token: " + refreshedToken);
        SharedPreferences sh = getSharedPreferences("fcm_token", MODE_PRIVATE);
        SharedPreferences.Editor editor = sh.edit();
        editor.putString("fcm_token", refreshedToken);
        editor.commit();

    }
}
